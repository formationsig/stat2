# Sommaire

* [Introduction](README.md)
* [Module stat 2.1 - Comparer plusieurs séries de données : Y-a-t-il une relation entre les séries (statistiques) ?](mod21_stat_bivariee_chap1.md)
	* [1. Rappels, vocabulaire et théorie](mod21_stat_bivariee_chap1.md#1-rappels-vocabulaire-et-théorie)
	  * [1.1. Rappel du socle 1 (types de variables, opérateurs de position, de dispersion)](mod21_stat_bivariee_chap1.md#11-rappel-du-socle-1-types-de-variables-opérateurs-de-position-de-dispersion)
	  * [1.2. La statistique bivariée : un peu de théorie](mod21_stat_bivariee_chap1.md#12-la-statistique-bivariée--un-peu-de-théorie)

	* [2. Variables Quantitatives](mod21_stat_bivariee_chap2.md#2-variables-quantitatives)
	  * [2.1. Conditions de validité remplies: La distribution d'une des deux variables suit une loi normale](mod21_stat_bivariee_chap2.md#21--conditions-de-validit%C3%A9-remplies-la-distribution-dune-des-deux-variables-suit-une-loi-normale)
	  * [2.2.  Conditions de validité non remplies: la distribution d'une des deux variables NE suit PAS une loi normale](mod21_stat_bivariee_chap2.md#22--conditions-de-validit%C3%A9-non-remplies-la-distribution-dune-des-deux-variables-ne-suit-pas-une-loi-normale)
	  * [2.3.  Exercice de restitution](mod21_stat_bivariee_chap2.md#23--exercice-de-restitution)
	  * [2.4.  Aller plus loin: Calculer la formule de corrélation et la représenter graphiquement](mod21_stat_bivariee_chap2.md#24--aller-plus-loin-calculer-la-formule-de-corr%C3%A9lation-et-la-repr%C3%A9senter-graphiquement)
	    * [2.4.1. La droite de régression](mod21_stat_bivariee_chap2.md#241-la-droite-de-r%C3%A9gression)
	    * [2.4.2. Ajouter un critère discriminant](mod21_stat_bivariee_chap2.md#242-ajouter-un-crit%C3%A8re-discriminant)
	    * [2.4.3 Plusieurs variables quantitatives: lire une ACP](mod21_stat_bivariee_chap2.md#243-plusieurs-variables-quantitatives-lire-une-acp)
	  
	* [3. Deux séries d’une même variable quantitative]( mod21_stat_bivariee_chap3.md#3-deux-séries-dune-même-variable-quantitative)
	  * [3.1 Dans le cas où les conditions sont remplies](mod21_stat_bivariee_chap3.md#31-dans-le-cas-où-les-conditions-sont-remplies)
	  * [3.2. Dans le cas où les conditions NE sont PAS remplies](mod21_stat_bivariee_chap3.md#32-dans-le-cas-où-les-conditions-ne-sont-pas-remplies)
	  *  [3.3. Test de Student: schéma de décision](mod21_stat_bivariee_chap3.md#33-test-de-student-schéma-de-décision)
	  *  [ 3.4. Exercice de restitution](mod21_stat_bivariee_chap3.md#34-exercice-de-restitution)
	  *  [ 3.5. Pour aller plus loin : l'ANOVA](mod21_stat_bivariee_chap3.md#35-pour-aller-plus-loin--lanova)
	* [4. Variables Qualitatives](mod21_stat_bivariee_chap4.md#4-variables-quantitatives)
	
* [Module stat 2.1 - Correction des exercices + scripts R](mod21_stat_bivariee_correction.md)