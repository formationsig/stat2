# 4. Deux variables qualitatives

[![diapo 4.](images/Dia_400.png "diapo 4.")](https://slides.com/archeomatic/stat_22/fullscreen#/7)

## 4.1 Rappels

**Variables qualitatives** : 
Ce sont des données, des variables, renseignant des champs descriptifs. Exemples : type de structure, morphologie de sépultures, faciès céramique, type de débitage lithique…

### la représentation des données qualitatives

<u>exemple</u> : catégorie fonctionnelle de céramique (410_ceram_categorie_fonction.csv)

Dans le tableau la colonne fonction contient des types. Les variables qualitatives ne permettent pas de calcul direct avec des indicateurs de tendance. Pour exploiter ces données, il faut commencer par compter via un tableau de dénombrement.

``` R
table(ceram$Fonction)
```


``` R
préparation     Service    stockage 
         57         896         378 
```

Cela nous donne le décompte absolue. Pour une lecture proportionnelle des quantités, nous pouvons réaliser un tableau de dénombrement en fréquence relative, ici en %.

```R
table(ceram$Fonction)/length(ceram$Fonction)*100
```

```
préparation     Service    stockage 
   4.282494   67.317806   28.399699
```

:eyes: Nous constatons ici que la majorité de l'effectif (environ 2/3) est occupé par de la vaisselle de service, moins d'un tiers est de la céramique de stockage alors que la vaisselle de préparation est en minorité (moins de 5%).

La représentation graphique associée à ce type de donnée est le diagramme en barre.

![catégorie fonctionnelle](images/410_cat_ceram.png)

Il est également possible de représenter les fréquences relatives à conditions de préciser les effectifs, ou *a minima* l'effectif total.

```R
pie(table(ceram$Fonction), col=c("white","grey","black"), main="catégorie fonctionnelle de céramique \n (n=1331)")
```

![camenbert catégories fonctionnelles](images/410_cat_fonc_camenbert.png)

Pour aller plus loin, nous allons pouvoir comparer ces répartitions avec d'autres ou avec un modèle théorique.



<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>



## **4.2 Application** : tester le lien entre deux variables qualitatives

<u>exemple utilisé</u> : sexe et catégorie à l'Inrap (420_cat_sex.csv)

### Le test du Khi²

Cette méthode fut proposée par Karl Pearson en 1900 dans l'article :

*"Sur le critère de décider si, dans le cas d'un système de variables en corrélation, un ensemble donné de déviations par rapport à la valeur probable est tel qu'il peut être raisonnablement supposé avoir été obtenu par un échantillonnage au hasard."*[^1]

[^1]: **Pearson 1900** PEARSON (K.) — On the criterion that a given system of deviations from the probable in the case of a correlated system of variables is such that it can be reasonably supposed to have arisen from random sampling. *The London, Edinburgh, and Dublin Philosophical Magazine and Journal of Science*, 50, 302, 1900, p. 157‑175.

Ici, le titre de la publication nous indique quelques éléments essentiels :

d'abord un système de décision : *"Sur le critère de décider si"*

Permettant de déterminer si une déviation peut être expliquée par le hasard (hypothèse *H<sub>0</sub>*) : *ensemble donné de déviations par rapport à la valeur probable est tel qu'il peut être raisonnablement supposé avoir été obtenu par un échantillonnage au hasard."*

Le fonctionnement est identique à celui de tous les test d'hypothèse avec la même démarche :**1 : poser l’hypothèse de travail
2 : définir le seuil de significativité
3 : choisir le test
4 : lire le résultat
5 : tirer des conclusions**

avec vérification des conditions du test du khi² : au moins **5 individus** par combinaison de modalité et un **effectif total supérieur à 30**, valeurs discutées et discutables.
Pour la combinaison de deux variables **ne possédant que 2 modalités** chacune (par exemple la comparaison de deux distributions par sexe : Femelle/mâle), il faut appliquer la **correction de Yates** : on retire 0,5 à chaque soustraction entre valeurs observées et théoriques.

#### Application étape par étape

L'objectif de cet exercice est d'étudier les deux variables qualitatives, catégorie et sexe, au sein de l'institut. Pour cela nous allons tout d'abord résumer ces données, tester le lien qu'elles pourraient avoir et son degré de significativité.

Commençons par poser les hypothèses et fixer le seuil alpha :
{% hint style='tip' %}

- *H<sub>0</sub>* : les écarts de répartition des sexes par catégorie salariale à l'Inrap peuvent être expliquées par des variations aléatoires.
- *H<sub>1</sub>* : les écarts de répartition des sexes par catégorie salariale à l'Inrap ne sont pas le fait de variations aléatoires.

Le seuil de significativité est fixé à la valeur courante de 5% : alpha = 0,05
{% endhint %}

Commençons le test du khi² par une première représentation est un tableau de comptage (ou de dénombrement)

```R
 table(tab$sexe,tab$categorie)
```

|      | 2    | 3    | 4    | 5    |
| ---- | ---- | ---- | ---- | ---- |
| F    | 124  | 269  | 210  | 79   |
| H    | 149  | 407  | 266  | 140  |

:eyes: A partir de ce tableau il est difficile d'avoir une idée de l'équilibre des sexes par catégorie, étant donné que les effectifs par sexe ne sont pas égaux.

Essayons la représentation graphique associée, le diagramme en barre.

```R
barplot(table(tab$sexe,tab$categorie),beside=T, col=c("pink","blue"),ylab= "Effectif", xlab="Catégorie")
legend("topleft",fill=c("pink","blue"), legend=c("femme","homme"))
```

![500_cat_sex](images/420_cat_sex.png)

:eyes: Là encore, le déséquilibre des effectifs des deux sexes ne permet pas une lecture aisée. On constate qu'il y a moins de femmes dans toutes les catégories.

|       | 2    | 3    | 4    | 5    | total |
| ----- | ---- | ---- | ---- | ---- | ----- |
| F     | 124  | 269  | 210  | 79   | 682   |
| H     | 149  | 407  | 266  | 140  | 962   |
| total | 273  | 676  | 476  | 219  | 1644  |

Essayons de calculer la proportion des catégories au sein des deux sexes.

```R
prop.table(table(tab$sexe,tab$categorie),margin=1)
```

```
            2         3         4         5
  F 0.1818182 0.3944282 0.3079179 0.1158358
  H 0.1548857 0.4230769 0.2765073 0.1455301
```

Ce tableau montre les fréquences dans les quatre catégories pour chaque sexe.

:eyes: On note des déséquilibres à première vue assez légers, de l'ordre de 3 % par catégorie. Il faut désormais déterminer si ces différences sont significatives (*i.e.* ont peu de chances d'être expliquées par des variations au hasard).

Pour cela, il nous faut une référence un modèle auquel comparer notre distribution. Ce modèle n'existe pas, il va falloir le créer. A partir des effectifs réels dans chaque combinaison de modalité des deux variables, nous allons calculer la valeur théorique lorsque sexe et catégorie professionnelle sont totalement indépendants.

Pour cela il faut calculer "les marges" (c'est à dire les totaux) des colonnes et lignes de notre tableau de dénombrement. Ces effectifs dit "à l'indépendance" se calculent de manière simple en multipliant le total d'une ligne avec le total d'une colonne et en divisant par le total général. Par exemple, pour la combinaison de modalités "femme" et "catégorie 2", nous multiplions la fréquence observée des femmes dans notre échantillon (682) par la fréquence observée des agents de catégorie 2 (273) divisé par le total (1644) soit 113.25.  On applique ce calcul à chaque cellule du tableau.

```matlab
 Σ ligne × Σ colonne / Σ total
```

|       | 2      | 3      | 4      | 5      | total |
| ----- | ------ | ------ | ------ | ------ | ----- |
| F     | 113,25 | 280,43 | 197,46 | 90,85  | 682   |
| H     | 159,75 | 395,57 | 278,54 | 128,15 | 962   |
| total | 273    | 676    | 476    | 219    | 1644  |

Ce nouveau tableau donne les effectifs dans le cas d'une indépendance parfaite entre les deux variables. 

:eyes: ​Il est possible de le lire ainsi : si le sexe n'avait aucune influence sur la catégorie professionnelle, il devrait y avoir 133 femmes et 160 hommes dans la catégorie 2.

Pour comparer avec les effectifs réels, nous allons soustraite au tableau des effectifs observés celui des effectifs à l'indépendance et ainsi obtenir le tableau des écarts à l'indépendance[^2].

[^2]: il est inutile de calculer les marges de ce tableau qui sont par construction égales à 0.

|      | 2      | 3      | 4      | 5      |
| ---- | ------ | ------ | ------ | ------ |
| F    | 10,75  | -11,43 | 12,54  | -11,85 |
| H    | -10,75 | 11,43  | -12,54 | 11,85  |

:eyes: Ce tableau est facile à lire. Par exemple, pour la catégorie 2, il faudrait 10 hommes de plus (soit 10 femmes de moins) pour être à l'indépendance.

Cette vision n'est toutefois pas encore assez juste. Par exemple, l'écart à l'indépendance pour les femmes en catégorie 2 et 4 est comparable (10,75 et 12,54). Or l'effectif des agents dans ces deux catégories n'est pas du tout le même avec presque 2 fois plus d'agent en catégorie 4 qu'en catégorie 2. En fréquence relative, cet écart à l'indépendance est deux fois plus important pour la catégorie 2.

Il nous reste à calculer la contribution au khi² de chaque combinaison de modalité. Pour cela, il suffit d'élever au carré chaque écart à l'indépendance et de diviser par l'effectif théorique à l'indépendance.

```R
χ2= Σ(Effectif observé - Effectif théorique)²/Effectif théorique
```

|       | 2    | 3    | 4    | 5    | total |
| ----- | ---- | ---- | ---- | ---- | ----- |
| F     | 1,02 | 0,47 | 0,80 | 1,55 | 3,83  |
| H     | 0,72 | 0,33 | 0,56 | 1,10 | 2,71  |
| total | 1,74 | 0,80 | 1,36 | 2,64 | 6,54  |

Les contributions au khi² permettent de voir là où les déséquilibres sont les plus importants, ici dans les catégories 2 et 5, le plus gros écart concernant les femmes de catégorie 5.

La somme total de ces contributions correspond à la valeur du khi² à comparer avec la table de référence.

{% hint style='working' %}
Celle-ci est disponible dans le dossier documentation fourni ou  facilement accessible dans l'internet.
{% endhint %}

Pour lire la valeur de référence dans ce tableau, il faut connaître le seuil de significativité (alpha, défini plus haut) et le degré de liberté. Dans le cas d'un test du Khi², il correspond au nombre de modalité moins 1 de la première variable, multiplié par le nombre de modalité moins une de la seconde variable : *(ncolonne-1)x(nligne-1))*
Une manière plus simple de l'exprimer est le nombre de possibilités qu'a un individu d'avoir des modalités entièrement différentes des siennes. Par exemple, si je suis une femme en catégorie salariale 3, les possibilités restantes et entièrement différentes sont d'être un homme en catégorie salariale 2, 4 ou 5 soit un degré de liberté de 3.

![table du khi²](images/420_table_du_khi2_5_3.png)

A l'intersection de ces deux entrées (0,05 et 3), la table nous fournit une valeur de 7,81. Cette valeur de khi² correspond au maximum pouvant être expliqué par des variations aléatoires au seuil de 5% avec un degré de liberté de 3.
Le khi² observé y est inférieur, ce qui signifie que les écarts observés dans mon tableau peuvent être dues à des variations "au hasard". L'hypothèse nulle ne peut pas être rejetée.

Au seuil d'erreur de 5%, les différences de catégories salariales entre hommes et femmes à l'INRAP peuvent être expliquées par le hasard.

{% hint style='tip' %}
Si on ne peut exclure ici que ces différences soient totalement liées à des variations aléatoires, on ne peut pas affirmer qu'elles ne sont liées qu'à ses variations.
{% endhint %}


#### Application dans R

Appliquons le même processus avec R

```R
#j'importe le tableau des données brutes après avoir définis l'espace de travail
#dans un objet dénommé "tab"
tab <- read.csv2("cat_sex.csv" #nom du fichier
	,stringsAsFactors = T) #option pour la reconnaissance automatique des variables catégorielles
	#L'automate n'étant qu'un automate, il faut lui préciser que la variable "catégorie" dont les modalités sont des chiffres est qualitative.
	#on transforme le type de cette variable avec la fonction as.factor
tab$categorie <- as.factor((tab$categorie))
#je réalise le tableau de contigence
table(inrap$sexe,inrap$categorie)
```

Ce tableau permet d'avoir un premier aperçu des effectifs de chaque combinaison et donc de la distribution.
Il permet également de vérifier si une des deux conditions du test du khi² est remplies :
**1 un effectif supérieur ou égal à 5 dans chaque combinaison de modalité**
Pour vérifier la seconde :
**2 un effectif total de plus de 30 individus**
demandons à R l'effectif totale de notre tableau :

```R
#effectif total en demandant la longueur d'une des colonnes du tableau

length(tab$sexe)
```

La seconde condition est remplie, nous pouvons réaliser le test du khi²

```R
#test du khi² entre sexe et catégories
chisq.test(tab$sexe, tab$categorie)
```

![résultat du khi²](images/420_resultat_chi2.png)

Le résultat précise que le test du khi² de Pearson a été effectué, rappel les données d'entrée, donne la valeur du khi² : 6,54 comme dans notre précédent calcul, le degré de liberté et enfin la valeur p, ici de 0,08805.
La commande permet d'avoir un résultat rapide mais nous pouvons accéder aux étapes et détails du calcul en enregistrant le test dans un objet :

```R
#stockons le test dans un objet nommé "res"
#pour avoir accès aux détails
res <- chisq.test(tab$sexe, tab$categorie)
```

Cette objet contient  :

```R
#la valeur du khi²
res$statistic
#le degré de liberté
res$parameter
#la valeur p
res$p.value
#la méthode
res$method
#le nom des variables
res$data.name
#le tableau de contigence d'entrée (valeurs observées)
res$observed
#le tableau des valeurs attendues
res$expected
#le tableau des résidus de Pearson
res$residuals
#et des résidus standards
res$stdres
```



{% hint style='info' %}
Les résidus sont des indicateurs qui pondèrent l'écart à l'indépendance par les effectifs. Le test dans R en calcul deux types :

- les résidus de Pearson : (observée-théorique)/√théorique pour chaque combinaison de modalité

- les résidus standards : (observée-théorique)/√théorique(1-effectif d'une des modalité)x(1-effectif de l'autre modalité). Ces derniers sont centrés et réduits (moyenne de 0 et écart-type de 1) pour être comparables entre plusieurs tests.

  Ces indicateurs ne nous intéressent pas ici, il est plus simple de lire les contributions au khi²
  {% endhint %}

  

### Le test exact de Fisher

Lorsqu'une des conditions pour pratiquer le test du khi² n'est pas remplie, il est possible d'utiliser le test exact de Fisher. La seule condition est que l'effectif de chaque combinaison possible de modalité soit supérieure à 0. Il est préféré pour les petits effectifs mais peut s'appliquer à n'importe quel échantillon. Ce test implique des calculs factoriels que même les ordinateurs modernes auront du mal effectuer si le nombre de modalités ou leurs effectifs sont grands.
Il est dénommé test "exact" car contrairement aux autres tests vus jusqu'ici, il ne renvoie pas un coefficient à comparer à des tables mais calcule et délivre directement la probabilité p.

Dans R il est utilisé comme ça :

```R
fisher.test(tab$sexe,tab$categorie)
```

Ce qui livre pour nos données le résultat suivant :

![résultat Fisher](images/420_fisher_sex_cat.png)

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

### Exercice

{% hint style='tip' %}
A partir du tableau 4.2_ceram_categorie_fonction.csv :

- décrivez les données
- étudiez le lien entre la catégorie fonctionnelle et le lieu de production des contenants en repectant toutes les étapes de la procédure
  {% endhint %}
  

{% hint style='working' %}
Correction effectuée avec R ou directement dans un tableur, comme pour l'exercice précédents.
{% endhint %}

```R
#je définis l'espace de travail
setwd("C:/Stat2/donnees_exercices")
#j'importe les données dans un objet "cer"
cer <- read.csv2("53_ceram_categorie_fonction.csv", stringsAsFactors = T)
#résumé des données
summary(cer)
#affichage de l'effectif total
length(cer$Production)
[1] 1331
```

| Production                  |      |
| --------------------------- | ---- |
| Importation gauloise        | 407  |
| Importation méditerranéenne | 124  |
| Production Locale           | 649  |
| Production régionale        | 151  |

| Fonction    |      |
| ----------- | ---- |
| préparation | 57   |
| Service     | 896  |
| stockage    | 378  |

Cette première étape permet d'avoir une vision de la répartition des différentes modalités des deux variables : beaucoup de production locale, peu de vaisselle de préparation...

Il est temps de faire le tableau de contingence :

```R
#tableau croisé des fonctions et productions
table(cer$Production,cer$Fonction)
```

|                             | préparation | Service | stockage |
| --------------------------- | ----------- | ------- | -------- |
| Importation gauloise        | 5           | 380     | 22       |
| Importation méditerranéenne | 9           | 27      | 88       |
| Production Locale           | 30          | 366     | 253      |
| Production régionale        | 13          | 123     | 15       |

Maintenant que nous avons une bonne vision des données, nous pouvons déballer la procédure :
1- Les hypothèses :
*H<sub>0</sub>* : la répartition des différents types fonctionnels de céramique est indépendante de leur lieu de production
*H<sub>1</sub>* : catégorie fonctionnel et lieu de production sont liés

2 - le seuil : alpha = 0.05

3 - le test : les conditions sont remplies pour un test du khi² : effectif important et effectif minimal par croisement de modalité de 5. Le nombre de modalité par variable est supérieur à 2, il n'y a pas de correction à appliquer. Le test exacte de Fisher conviendrait également mais comme le nombre de modalités et l'effectif sont élevés, R va peiner à calculer le résultat.

```R
#test du khi²
chisq.test(cer$Production,cer$Fonction)
```

![résultat_chisq_cer](images/420_resultat_chi2_cer.png)

4 - lecture du résultat :

Au seuil d'erreur de 5%, *H<sub>0</sub>* est rejetée.

5 - interprétation :

Il y a un lien significatif entre la fonction des céramiques et leur lieu de production. Un examen des écarts à l'indépendance permet de voir le poids de chaque combinaison de modalité dans ce résultat :

```R
# je stocke les résultats dans un objet "res"
res <- chisq.test(cer$Production,cer$Fonction)
# pour obtenir le tableau des contributions au khi² arrondies aux entiers
round(						#arrondi des valeurs du tableau
  res$observed-res$expected #calcul des écarts à l'indépendance
  ,0)						#avec 0 chiffre après la virgule
```

|                             | préparation | Service | stockage |
| --------------------------- | ----------- | ------- | -------- |
| Importation gauloise        | -12         | 106     | -94      |
| Importation méditerranéenne | 4           | -56     | 53       |
| Production Locale           | 2           | -71     | 69       |
| Production régionale        | 7           | 21      | -28      |

Les plus forts écarts sont constatés entre importation gauloise et stockage ou service, dans des directions opposées. Idem pour les productions locales dans le sens opposé. La lecture de grands tableaux de contigence n'est pas toujours aisée. Pour aller plus loin, il existe l'AFC.

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

## 4.3 Pour aller plus loin : l'AFC

L'analyse factorielle des correspondances fut développée dans les années 70 par Jean-Paul Benzécri. Il s'agit de l'équivalent des ACP pour des variables qualitatives. Son calcul est basé sur les valeurs de khi².
Cette partie propose une vue rapide pour permettre une première compréhension de la lecture d'une analyse factorielle. Nous allons utiliser l'exemple des fonctions et provenance de céramiques.
Dans R :

```R
#je définis l'espace de travail
setwd("C:/Stat2/donnees_exercices")
#j'importe les données céram
cer <- read.csv2("53_ceram_categorie_fonction.csv"
                 ,stringsAsFactors = T)
#je charge les outils
library(FactoMineR)
library(explor)
#je réalise l'AFC et enregistre les résultats dans un objet "resAFC"
resAFC <- CA(table(cer$Production,cer$Fonction))
#je visualise un beau graphique
explor(resAFC)
```

Le premier graphique est l'histogramme des valeurs propres. Il indique la quantité d'information conservée par la projection sur chaque axe. Ici le premier axe (horizontal) contient à lui seul 95 % des résultats.

![valeurs_propres](images/430_valeurs_propre_AFC.png)

Le second axe contient le reste. Si ces valeurs étaient faibles, il serait inutile de continuer à lire graphique individus/variables.

![graph_AFC](images/430_ind_var_AFC.png)

Ici, nous ne regarderons que le premier axe. Il oppose très clairement vaisselle de stockage et de service ainsi qu'importation méditerranéenne et importation gauloise/production locale, à l'instar de ce qui fut constaté pour les écarts à l'indépendance. Les productions locales sont plutôt liées à la vaisselle de préparation. La proximité des points sur un axe signifie ici l'association et l'éloignement, la distanciation.

![tab_AFC](images/430_tab_AFC.png)

Le dernier onglet donne accès au tableau des coordonnées par axe, résume les contributions et fourni la qualité de la projection via les cosinus².
