# 2. Variables Quantitatives

[![diapo 2.1](images/Dia_210.png "diapo 2.1")](https://slides.com/archeomatic/stat_22/fullscreen#/4/1)


Nous avons deux variables quantitatives dont nous voulons connaître la relation : la corrélation.

**La corrélation**

L'analyse de corrélation est une technique statistique qui vous donne des informations sur la relation entre deux variables quantitatives. La force de la corrélation est déterminée par un coefficient (*rho* ou *cor*) qui varie de -1 à +1. Les analyses de corrélation peuvent donc être utilisées pour déterminer la force et la direction de la corrélation.

<u>Corrélation positive</u>:
Une corrélation positive existe si des valeurs plus importantes de la variable A s'accompagnent de valeurs plus importantes de la variable B. La taille et la pointure des chaussures, par exemple, sont corrélées positivement et un coefficient de corrélation proche de 1 en résulte, c'est-à-dire une valeur positive.

<u>Corrélation négative</u>:
Une corrélation négative existe si des valeurs plus grandes de la variable A sont accompagnées de valeurs plus petites de la variable B. Le prix du produit et la quantité vendue ont généralement une corrélation négative ; plus un produit est cher, plus la quantité vendue est faible. Dans ce cas, le coefficient de corrélation est compris entre -1 et 0, il prend donc une valeur négative.



{% hint style='danger' %}

<u>Rappel:</u> **corrélation n'est pas causalité !** La causalité se produit lorsqu'une variable en affecte une autre, tandis que la corrélation implique simplement une relation entre les deux variables.

{% endhint %}



Nous allons utiliser 2 tests selon si les variables suivent une distribution normale :

→ La distribution de la variable **suit une loi normale** et peut être résumée par les paramètres caractérisant une loi normale, à savoir la moyenne et la variance.

- On effectuera un **test paramétrique → test de Pearson**

**→** La distribution de la variable **ne suit pas à une distribution “normale”/aléatoire**, on ne pourra pas caractériser cette distribution par des paramètres.

- On effectuera un **test non paramétrique → test de Spearman**

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

### 2.1.  Conditions de validité remplies: La distribution d'une des deux variables suit une loi normale

{% hint style='tip' %}

* Travailler sur le jeu de données **211_insee_france_pop.csv**

* Décrire le tableau et formuler des questions que l'on pourrait poser.

{% endhint %}

```R
#J’ouvre mon tableau de données : 
insee <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/211_insee_france_pop.csv")
#Je regarde la structure du tableau
str(insee)
```



**Étape 1 : Poser l’hypothèse *H<sub>0</sub>***

La corrélation que l’on peut supposer entre le nombre de femmes et celui des hommes sur les 10 dernières années est due au hasard.



**Étape 2 : Définir le seuil de significativité**

Après avoir posé nos hypothèses, on va accepter un risque d’erreur. En statistique, on n’est jamais sûr à 100%, on accepte toujours un risque de se tromper: avec un seuil à 95% , on peut dire *"je suis sûre à 95% du rejet ou non"*.



**Étape 3 : Choisir le test**

{% hint style='danger' %}

Il faut vérifier que les distributions suivent une loi normale pour choisir le test de corrélation:

* :ok: On peut utiliser le test de Bravais-Pearson
* :x: On utiliser le test de Spearman

{% endhint %}

Plusieurs méthodes sont possible:

1. L'histogramme
2. Le diagramme Quantile-Quantile
3. Le test de Shapiro



​	<u>Méthode 1:</u> L'histogramme



La 1ère chose à faire est de représenter graphiquement les 2 variables pour voir à l’œil  nu si leur distribution dessine une courbe "en forme de cloche"  qui se rapprocherai d'une **courbe de Gauss**.

```R
# tracer un histogramme de la variable "femmes"
hist(insee$femmes)  
# tracer un histogramme de la variable "hommes"
hist(insee$hommes)
```

![211_hist_femmes](images/211_hist_femmes.png)  ![211_hist_hommes](images/211_hist_hommes.png)

:eyes: On n’identifie pas vraiment des formes de cloches. Mais cela est très subjectif, cela s’explique peut-être par un faible effectif (ici, le nombre d’années prises en compte). C'est pourquoi Il ne faut pas se limiter à cette seule approche.



​	<u>Méthode 2:</u> Le diagramme Quantile-Quantile



ll s’agit de comparer la distribution observée de notre variable à un modèle théorique inventé pour l’occasion qui suit une loi normale (un effectif aléatoire avec la mêmes moyenne et la même variance que la variable à comparer).
L’axe des ordonnées correspond aux valeurs des modalités de la variable observée, classées par ordre croissant. L’axe des abscisses porte les Quantiles théoriques. Le nombre de ces derniers est égal à l’effectif observé (11). 

> <u>*Note:*</u> Dans le cas présent, ils sont par ailleurs centrés (moyenne=0) et réduits (sd=1) mais ce n’est pas forcément nécessaire pour apprécier leur alignement.

Si les points s’alignent, le modèle théorique est validé, la normalité de la distribution des modalités de la variable observée est admise. 

```R
# Instalation et utilisation de la bibliopthèque de fonction ggpubr
install.packages("ggpubr")
library(ggpubr)
# tracer du diagramme QQ de la variable "femmes"
ggqqplot(insee$femmes)+labs(title="femmes")
# tracer du diagramme QQ de la variable "hommes"
ggqqplot(insee$hommes)+labs(title="hommes")
```

![qqplot femmes](images/211_qqplot_femmes.png) ![qqplot hommes](images/211_qqplot_hommes.png)

:eyes: Les points s’alignent : les distributions suivent une loi normale. Avec la fonction utilisée, un intervalle de confiance à 95%, matérialisé par une bande grise, aide à la lecture.




​	<u>Méthode 3:</u> Le test de Shapiro

Les deux méthodes graphiques précédentes donnant des indications différentes, essayons une 3e approche, plus "mathématique": le test de Shapiro.

> Note: Comme il s’agit d’un test, la procédure présentée pour tous les tests s’appliquent également. (tests gigognes :dolls: ) 

 * Étape 1 : Poser l’hypothèse **H<sub>0</sub>**: 

   *H<sub>0</sub>* : la distribution est normale

 * Étape 2 :Définir un seuil de significativité

      𝜶 = 0,05
      
 * Étape 3 : Réaliser le test


```R
shapiro.test(insee$femmes)
```

```
Shapiro-Wilk normality test
      
data:  insee$femmes
W = 0.97438, p-value = 0.927
```

```R
shapiro.test(insee$hommes)
```
```
Shapiro-Wilk normality test
          
data:  insee$hommes
W = 0.97843, p-value = 0.9568
```

* Étape 4 : Lire le résultat

​	Pour les 2 variables **la p-value est supérieur au seuil de significativité choisi de 0.05** (p-value > 𝜶 ) on peut donc accepter l'hypothèse nulle (*H<sub>0</sub>*)

 * Étape 5 : Tirer des conclusions

      Les distributions des modalités de la variable “hommes” comme de la variable “femmes” suivent une loi normale (L’histogramme n'évoque que moyennement une courbe en forme de cloche, en revanche les quantiles sont alignés dans la zone grise et *H<sub>0</sub>* est acceptée avec le test de Shapiro)



**Étape 4 : Réaliser le test : la corrélation de Pearson**

→ Les conditions de normalité étant remplies, on peut utiliser **le test de corrélation linéaire de Bravais-Pearson** (qui utilise les variances = rappel : attention aux erreurs de mesures et aux outliers) :

```R
# réaliser le test de correlation de Pearson
cor.test(insee$femmes,insee$hommes,method="pearson")
```

```
Pearson's product-moment correlation

data:  insee$femmes and insee$hommes
t = 51.058, df = 9, p-value = 2.129e-12
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.9931335 0.9995691
sample estimates:
      cor 
0.9982783
```



**Étape 5 : Interpréter le résultat**

Si la p-value est inférieure à alpha, le résultat du test (rho) est statistiquement significatif. On peut donc s’y référer pour réfléchir sur la corrélation entre les deux variables.

Si p-value est supérieure à 𝜶, alors le résultat du test n’est statistiquement pas significatif. Il est risqué de l’utiliser pour savoir si les deux variables sont corrélées ; les données ne permettent pas de soutenir l’une (*H<sub>0</sub>*) ou l’autre (*H<sub>1</sub>*) des hypothèses.

Dans le cas présent, p-value étant (bien) inférieure à 𝜶, il est intéressant de se pencher sur rho.

Le principe :

- Plus rho/cor est proche de 1, plus relation il y a et celle-ci est positive : les deux variables bougent dans la même direction ; 
- Plus Rho/cor est proche de -1, plus relation il y a mais celle-ci est négative : les deux variables bougent dans une direction opposée.
- Une corrélation égale à 0 signifie que les variables ne sont pas corrélées linéairement

:eyes: Avec Rho = 0,998, le nombre d’hommes et de femmes sont nettement corrélées, une corrélation en outre positive (c'est à dire que le nombre d’hommes augmente proportionnellement à celui des femmes).



Mais Comment faire si les deux distributions ne suivent pas une loi normale?

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

### 2.2.  Conditions de validité non remplies: la distribution d'une des deux variables NE suit PAS une loi normale

{% hint style='tip' %}

Travailler sur le jeu de données **212_bavay_dim_fosse.csv**

{% endhint %}

```R
# importer le tableau
bavay <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/212_bavay_dim_fosse.csv")
# regarder la structure du tableau : 
str(bavay)
```

```
'data.frame':	62 obs. of  12 variables:
... 
 $ prof      : int  5 10 70 100 96 130 16 88 67 70 ...
 $ alti_surf : num  138 139 137 137 137 ...
 $ alti_fond : num  138 139 136 136 136 ...
...
```

Je décide de tester la corrélation entre la profondeur des sépultures et l’altitude des fonds



**Étape 1 : Poser l’hypothèse *H<sub>0</sub>***

La corrélation que l’on peut supposer entre profondeur et altitude de fond des fosses sépulcrales est dû au hasard.

**Étape 2 : Définir le seuil de significativité**

On fixe le seuil de significativité 𝜶 à 0.05

**Étape 3 : Choisir le test**

{% hint style='danger' %}

Il faut vérifier que les distributions suivent une loi normale pour choisir le test de corrélation:

* :ok: On peut utiliser le test de Bravais-Pearson
* :x: On utiliser le test de Spearman

{% endhint %}

Vérification de la normalité des distributions avec un test de Shapiro

```R
shapiro.test(bavay$alti_fond)
```

```
Shapiro-Wilk normality test
      
data:  bavay$alti_fond
W = 0.97775, p-value = 0.32
```

```R
shapiro.test(bavay$prof)
```
```
Shapiro-Wilk normality test
          
data:  bavay$prof
W = 0.93078, p-value = 0.001759
```

Pour la variable "profondeur" (`bavay$prof`) la p-value est inférieure au seuil de signification choisi (p-value < 𝜶 ) on peut donc rejeter l'hypothèse nulle (*H<sub>0</sub>*) → la distribution de cette variable ne suit donc pas une loi normale. 

**Étape 4 :Réaliser le test : la corrélation de Spearman**

L'une des variables ne suit pas une loi normale, je choisi donc de réaliser le test de corrélation de Spearman.

```R
# réaliser le test de correlation de spearman
cor.test(bavay$alti_fond,bavay$prof,method="spearman")
```
> <u>*Note:*</u> On change simplement le nom de la méthode à la fin de la ligne de commande

```
Spearman's rank correlation rho

data:  bavay$alti_fond and bavay$prof
S = 57105, p-value = 0.0003702
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
-0.438016 

Message d'avis :
Dans cor.test.default(bavay$alti_fond, bavay$prof, method = "spearman") :
  Impossible de calculer la p-value exacte avec des ex-aequos
```
>  <u>*Note:*</u> Le message rouge (`Message d'avis`) indique des valeurs égales dans une des variables (2 tombes ou + de même profondeur et/ou de même altitude de fond). 

**Étape 5 : Lire le résultat**

:eyes: Le résultat se lit de la même façon que pour le test de Pearson : une petite p-value donne de la significativité à rho. Ce dernier (-0,438) montre une corrélation moyenne, légèrement faible, des 2 variables.



### 2.3.  Exercice de restitution

{% hint style='tip' %}

* Continuer de travailler sur le jeu de données **212_bavay_dim_fosse.csv**
* Décrire le tableau
*  **Les longueurs et les largeurs de tombes sont liées ?**

{% endhint %}



> :information_source: Correction en fin de document

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

### 2.4.  Aller plus loin: Calculer la formule de corrélation et la représenter graphiquement



#### 2.4.1. La droite de régression

```R
# Tracer le nuage de points
plot(bavay$long,bavay$larg)
# le 1er argument = axe des abscisses & le 2nd argument = axe des ordonnées
```

![214_plot_long_larg](images/214_plot_long_larg.jpeg)

:eyes: Plus la corrélation est forte, plus les points sont alignés.

Plus les points sont alignés, plus ils dessinent une droite. On va donc essayer de tracer cette droite qui, dans souvent, va essayer de passer par un maximum de points ou, à défaut, le plus près possible de ceux-ci.

```R
# Tracer la droite de régression avec la fonction lm (linear model)
# le premier argument est la variable placée en ordonnées.
lm(bavay$larg~bavay$long)	 
```

```
Call:
lm(formula = bavay$larg ~ bavay$long)

Coefficients:
(Intercept)   bavay$long  
   -16.5334       0.4964
```

:eyes: R donne 2 chiffres qui correspondent à l’ordonnée à l’origine (b) et au coefficient directeur (a) de la droite de régression dont l’équation s’écrit (rappel du collège) Y=aX + b.

Dans le cas des tombes de Bavay, au regard de l’échantillon étudié, on peut dire que la longueur = 0.4964 x la largeur - 16,5334.

```R
# Représentation graphique
# Calculer des coordonnées de la droite de régression (les b et a dans Y=aX+b)
reg1<-lm(bavay$long~bavay$larg)
# Tracer le nuage de points
plot(bavay$larg,bavay$long)
# Ajouter la droite
abline(reg1,col=”red”)
```

![214_cor_test_lm.jpeg](images/214_cor_test_lm.jpeg)

La forme linéaire entre les deux variables est présupposée. Autrement dit, on fait l’hypothèse que la forme de la relation entre les variables est linéaire. 

Néanmoins, il est préférable de vérifier si cette hypothèse est acceptable, ou non, car si ce n’est pas le cas, les résultats de l’analyse n’auront pas de sens. 

Pour cela on va...

```R
# installer et appel de la bibliothèque de fonctions car (Companion to Applied Regression)
install.package("car")
library(car)
# Tracer la droite de regression et +
scatterplot(long~larg, data=bavay)
```

![214_cor_test_reg.jpeg](images/214_cor_test_reg.jpeg)



:eyes: Cette représentation:

* montre la distribution de chaque variable à l'aide de **boîtes à moustaches**

> <u>*Note:*</u> Les boîtes à moustaches montrent des *outliers* (=valeurs aberrantes) qui peuvent affaiblir la corrélation. 

*  montre la droite de régression ainsi qu'une courbe de régression non linéaire.

>  <u>*Note:*</u> La courbe à 2 pentes suggère non pas une mais 2 corrélations linéaires, soit potentiellement 2 sous-groupes. Tentons ici de trier les grandes tombes des petites.

#### 2.4.2. Ajouter un critère discriminant

```R
# Transformer la variable age_simpl(ifié) en variable catégorielle avec as.factor()
bavay$age_simpl <- as.factor(bavay$age_simpl)
# Tracer le nuage de points avec le critère age_simpl comme variable pour la couleur des points
plot(bavay$long,bavay$larg,
     pch=18,
     col=c("blue","orange","green")[bavay$age_simpl]
    )
# Ajouter une légende
legend("topleft",
       legend =c("NA","Adulte","Immature"),
       fill=c("blue","orange","green")
      )
```

![214_long_larg_age](images/214_long_larg_age.png)

:eyes: On voit bien la valeur aberrante en bas à gauche (un adulte dans une toute petite fosse) : le problème de mesure provient de la saisie ou de son recoupement sur le terrain.

```R
# sous-échantillonner la série en ne gardant que les immatures
immat <- subset(bavay,age_simpl=="immature")
# Calculer la droite de régression pour les immatures
reg2 <- lm(immat$larg~immat$long)
# L'ajouter au graphique
abline(reg2,col="green")
# idem avec un sous-échantillon des adultes
adulte <- subset(bavay,age_simpl=="adulte")
reg3 <- lm(adulte$larg~adulte$long)
abline(reg3,col="orange")
```

![214_cor_test_lm.color](images/214_cor.test_lm.color.jpeg)



On voit également qu’il y a bien 2 droites de régression. Une pour les immatures, les points en vert. Ceux-ci sont plutôt dispersés, illustrant une corrélation plutôt moyenne de la longueur et de la largeur. Une autre pour les adultes (en orange). Les points sont plus concentrés (malgré des valeurs étonnantes en haut à droite du graphique), la corrélation est plus forte.

On fait des **tests de normalité** de la distribution des 2 variables sous-échantillonnées selon l'âge simplifié avec le test Shapiro (pour les immatures puis pour les adultes) puis on fait le test approprié selon le résultat:

```R
shapiro.test(immat$long)
```

```
Shapiro-Wilk normality test
data:  immat$long
W = 0.9266, p-value = 0.2733
```

```
shapiro.test(immat$larg)
```

```
Shapiro-Wilk normality test
data:  immat$larg
W = 0.947, p-value = 0.5152
```

:eyes: Les 2 variables longueur et  largeur pour les immatures ont des distributions qui suivent la loi normale (p-value > 𝜶=0,05 : acceptation de *H<sub>0</sub>*).

```R
shapiro.test(adulte$long)
```

```
Shapiro-Wilk normality test
data:  adulte$long
W = 0.86981, p-value = 0.0001051
```

```R
shapiro.test(adulte$larg)
```

```
Shapiro-Wilk normality test
data:  adulte$larg
W = 0.95705, p-value = 0.088
```

:eyes: Pour les adultes, une des variables (`adulte$long`) a une distribution qui NE suit PAS une loi normale.



On peut faire les tests de corrélations adéquats :

Pour les immatures (2 distributions normales) on peut faire un test de Pearson

```R
cor.test(immat$long,immat$larg,method = "pearson")
```

```
Pearson's product-moment correlation

data:  immat$long and immat$larg
t = 2.1764, df = 12, p-value = 0.05021
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.001970984 0.828669185
sample estimates:
      cor 
0.5319941
```

Pour les adultes (1 des distributions NE suit PAS une loi normale)

```R
cor.test(adulte$long,adulte$larg,method = "spearman")
```

```
Spearman's rank correlation rho

data:  adulte$long and adulte$larg
S = 3543.2, p-value = 1.462e-10
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
0.7814843 

Message d'avis :
Dans cor.test.default(adulte$long, adulte$larg, method = "spearman") :
 Impossible de calculer la p-value exacte avec des ex-aequo
```

En séparant adultes et immatures les valeurs p sont moins bonnes (résultats moins significatif) malgré une meilleure corrélation chez les adultes (0.78 contre 0.74). Ce changement est probablement dû à la réduction des effectifs (groupe séparé en deux).

Le rapport longueur/largeur de fosse est moins constant chez les immatures : leurs fosses sont moins normées ou d’autres critères séparent plusieurs groupes.



Pour aller encore plus loin : enlevons du corpus les deux sépultures d’adultes qui présentent les longueurs les plus extrêmes (la plus petite et la plus grande) puis relançons le test de corrélation.

```R
# sous-échantillonage des adultes dont les fosses sépulcrales ont une longueur entre 150 et 280 cm
adulte2 <- subset(adulte,long>150&long<280)
# test de normalité
shapiro.test(adulte2$long)
```

```
Shapiro-Wilk normality test
data:  adulte2$long
W = 0.97582, p-value = 0.4761 
```

```R
shapiro.test(adulte2$larg)
```

```
Shapiro-Wilk normality test

data:  adulte2$larg
W = 0.93891, p-value = 0.02159
```

Le test de normalité (Shapiro) renvoie une p-value supérieure au seuil de significativité (𝜶=0,05) pour la variable `adulte2$larg`. On ne peut pas rejeter l' hypothèse *H<sub>0</sub>*  la normalité de la distribution, on utilise donc le test de corrélation de Spearman.

```
cor.test(adulte2$long,adulte2$larg,method="sperman")
```

```
	Spearman's rank correlation rho

data:  adulte2$long and adulte2$larg
S = 3513.6, p-value = 3.885e-09
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
0.7523894 

Message d'avis :
Dans cor.test.default(adulte2$long, adulte2$larg, method = "spearman") :
  Impossible de calculer la p-value exacte avec des ex-aequos
```

:eyes: La p-value est toujours basse mais le coefficient de corrélation a très légèrement diminué. Comme quoi, le test est quand même relativement robuste (il supporte, dans une certaine mesure, les valeurs extrêmes, pour ne pas dire aberrantes. De même si on se trompe et que l’on passe par la méthode de Spearman : rho =0.7523894) !

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

#### 2.4.3 Plusieurs variables quantitatives: lire une ACP

[![diapo 2.2](images/Dia_220.png "diapo 2.2")](https://slides.com/archeomatic/stat_22/fullscreen#/4/21)

##### 2.4.3.1. Objectif d’une ACP

Avec une **Analyse en Composantes Principales** (ACP), on cherche à extraire l’information pertinente contenue dans un tableau des données. Pour cela, on le résume en extrayant l’essentiel de sa structure en vue de faire des représentations graphiques à la fois fidèles aux données initiales et commodes à interpréter.

L’objectif de l’Analyse en Composantes Principales est de revenir à un espace de dimension réduite (par exemple, ici, 2) en déformant le moins possible la réalité. Il s’agit donc d’obtenir le résumé le plus pertinent des données initiales. Par analogie, on peut penser au photographe qui cherche le meilleur angle de vue pour transcrire en dimension 2 (le plan de sa photo) une scène située en dimension 3 (notre espace ambiant). La méthode mathématique va se charger de trouver l' ”angle de vue” optimal.

> Il existe une version plus détaillé [Pour en savoir plus - Comprendre l'ACP](https://docs.google.com/document/d/1MPL5S61ZKXxzuPzWjIZEerZPOizYtbPnZMOKTvJV_Fk/edit?usp=sharing)

##### 2.4.3.2. Lire une ACP

{% hint style='tip' %}
Travailler avec **243_acp_notes_matieres.csv**
{% endhint %}

Quand on se retrouve face à une ACP et pour la comprendre, il est indispensable de regarder plusieurs éléments et pas juste la représentation graphique. Il convient donc d’avoir à disposition le tableau des valeurs propres, le tableau et/ou le graphique des variables et les représentations graphiques. 

|             | math | phys | fran | angl |
| ----------- | ---- | ---- | ---- | ---- |
| stagiaire_1 | 6    | 6    | 5    | 5,5  |
| stagiaire_2 | 8    | 8    | 8    | 8    |
| stagiaire_3 | 6    | 7    | 11   | 9,5  |
| stagiaire_4 | 14,5 | 14,5 | 15,5 | 15   |
| stagiaire_5 | 14   | 14   | 12   | 12,5 |
| stagiaire_6 | 11   | 10   | 5,5  | 7    |
| stagiaire_7 | 5,5  | 7    | 14   | 11,5 |
| stagiaire_8 | 13   | 12,5 | 8,5  | 9,5  |
| stagiaire_9 | 9    | 9,5  | 12,5 | 12   |

On sait déjà comment analyser séparément chacune de ces 4 variables:

* en faisant un graphique
* en calculant des résumés numériques (paramètres de tendance centrale et de dispersion)

Nous savons également qu’on peut regarder les liaisons entre 2 variables (par exemple mathématiques et français):

* en faisant un graphique du type nuage de points
* en calculant leur coefficient de corrélation linéaire, voire en réalisant la régression de l’une sur l’autre

Mais, comment faire une étude simultanée des 4 variables, ne serait-ce qu’en réalisant un graphique ? 

La difficulté vient de ce que les individus (les élèves) ne sont plus représentés dans un plan, espace de dimension 2, mais dans un espace de dimension 4 (chaque élève étant caractérisé par les 4 notes qu’il a obtenues). 

{% hint style='working' %}
On va utiliser avec les packages  ![FactomineR](images/logo_factominer.png "logo factomineR") [FactomineR](http://factominer.free.fr/index_fr.html) et et ![exploR](images/logo_explor.png) [exploR](https://github.com/juba/explor).
{% endhint %}

```R
# Importer les données
acp<-read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/243_acp_notes_matieres.csv")
# Installer et activer les bibliothèques de fonction
install.package("FactoMineR")
library(FactoMineR)
install.package("explor")
library(explor)
# Calculer l'ACP avec FactomineR
res.PCA<-PCA(acp, quali.sup=1, graph=FALSE)
# Explorer l'ACP avec exploR
explor(res.PCA)
```

{% hint style='working' %}

La fenêtre **exploR** s'ouvre et permet d'explorer l'ACP grâce a un système d'onglets

{% endhint %}

**Tableau des valeurs propres**

![acp explor 1](images/222_acp_explor_1.png)

Le premier tableau de résultats à regarder est **le tableau des pourcentages d’inertie** correspondants aux différentes valeurs propres, contenant aussi les pourcentages cumulés associés : ce tableau va permettre de choisir la dimension *q* retenue pour interpréter l’A.C.P. Ici, les 2 premiers facteurs restituent à eux seuls la quasi-totalité de la dispersion du nuage (71,9 % et 28 %). 99,9 % des données sont donc représentés sur les deux premiers, ce qui permet de négliger les deux derniers. 



**Tableau et le graphique des variables**

![acp explor 2](images/222_acp_explor_2.png)

La technique de l’A.C.P. permet de calculer **les corrélations variables-facteurs**, autrement dit les coefficients de corrélation linéaire entre chaque variable initiale et chaque facteur retenu.

Dans un premier temps, ces quantités permettent un début d'interprétation des facteurs, dans la mesure où elles indiquent comment ils sont liées aux variables initiales. A ce stade, il est recommandé d’utiliser aussi la matrice des corrélations entre variables initiales, pour compléter cette interprétation.

Dans un second temps, les corrélations variables-facteurs permettent de réaliser **les graphiques des variables** dont l'étude détaillée conduit à préciser la signification des axes, c’est-a-dire des facteurs. On doit considérer uniquement le graphique selon les axes 1 et 2 si l’on a choisi *q* = 2 ; on doit au contraire considérer les 3 graphiques selon les axes 1 et 2, 1 et 3, 2 et 3, si l’on a choisi *q* = 3.

Pour notre exemple, en regardant le graphique et le tableaux de données des variables, on voit que le premier facteur est corrélé positivement, et assez fortement, avec chacune des 4 variables initiales : plus un élève obtient de bonnes notes dans chacune des 4 disciplines, plus il a un score élevé sur l’axe 1 ; réciproquement, plus ses notes sont mauvaises, plus son score est négatif ; l’axe 1 représente donc, en quelques sortes, le résultat global (dans l’ensemble des 4 disciplines considérées) des élevés. En ce qui concerne l’axe 2, il oppose, d’une part, le français et l’anglais (corrélations positives), d’autre part, les mathématiques et la physique (corrélations négatives). Il s’agit donc d’un axe d’opposition entre disciplines littéraires et disciplines scientifiques, surtout marqué par l’opposition entre le français et les mathématiques.

**Tableau et graphique des individus**

![acp explor 3](images/222_acp_explor_3.png)

Là encore, la technique de l’A.C.P. permet de calculer **les coordonnées des individus sur les axes**, leurs **contributions à la dispersion** selon chacun de ces axes (ainsi que leurs contributions à la dispersion globale, selon les *p* dimensions) et les **cosinus carrés**. Les coordonnées permettent de réaliser les **graphiques des individus** (1 ou 3 graphiques, selon que l’on a choisi *q* = 2 ou *q* = 3). Concernant ces graphiques, il faut tout d’abord noter que leurs axes s'interprètent de la même manière que les axes des graphiques des variables : les uns comme les autres sont associés aux facteurs.

En associant à ces graphiques les contributions des individus aux axes, on peut affiner l'interprétation de ces axes : chacun d’entre eux est surtout déterminé par les quelques individus présentant les plus fortes contributions ; ce sont en général ceux situés en position extrême sur l’axe, c’est-a-dire y ayant les plus fortes coordonnées, soit positives soit négatives. Bien sûr, avant d’utiliser un tel individu pour affiner l'interprétation d’un axe, il faut s’assurer que cet individu est bien représenté sur cet axe, autrement dit que le cosinus carré correspondant est grand (proche de 1).

:eyes: Pour notre exemple, on confirme ainsi que l’axe 1 représente le résultat d’ensemble des élevés : si on prend leur score (ou coordonnée) sur l’axe 1, on obtient le même classement que si on prend leur moyenne générale. 

Par ailleurs, l'élève “le plus haut” sur le graphique, celui qui a la coordonnée la plus élevée sur l’axe 2, est stagiaire_7 dont les résultats sont les plus contrastés en faveur des disciplines littéraires (14 et 11.5 contre 7 et 5.5). 

C’est exactement le contraire pour stagiaire_6 qui obtient la moyenne dans les disciplines scientifiques (11 et 10) mais des résultats très faibles dans les disciplines littéraires (7 et 5.5). 

On notera que stagiaire_4 et stagiaire_2 ont un score voisin de 0 sur l’axe 2 car ils ont des résultats très homogènes dans les 4 disciplines (mais à des niveaux très distincts, ce qu’a déjà révélé l’axe 1). 

L’axe 2 oppose bien les “littéraires” (en haut) aux “scientifiques” (en bas). 

