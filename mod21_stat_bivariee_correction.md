# Module stat 2.1 - Comparer plusieurs séries de données : Y-a-t-il une relation entre les séries (statistiques) ?

## Corrections des Exercices (Scripts R)





### 1.1.4. Rappel : utilisation de R [correction]

Le [Stat1 - module socle 1.2 - Initiation au traitement de données sous R](https://formationsig.gitlab.io/stat1/mod1_2_initR/mod1_2_initR.html) vous a permis de découvrir le langage R avec l'interface RStudio, c'est à dire importer un tableau, isoler une variable, traiter cette variable et la représenter.

{% hint style='working' %}
les fichiers sources sont tous au format **.csv**. Dans les scripts de corrections ils sont chargés avec l'instruction `read.csv2` et une *url* du type `https://gitlab.com/formationsig/stat2/-/raw/main/donnees/nomdufichier.csv`

avec `https://gitlab.com/formationsig/stat2/-/raw/main/donnees/` = le chemin vers dossier en ligne qui accompagne ce support de formation et `nomdufichier.csv` le nom du fichier à charger.

Il est évidemment possible de charger le fichier depuis un dossier en local en remplaçant le chemin vers dossier ou en spécifiant l'espace de travail au préalable.

{% endhint %}



```R
#Je définis mon espace de travail
setwd("C:/stat2/travail")
# Importer un CSV
sexstat <- read.csv2("114_rappel_sexe_stature.csv", stringsAsFactors = TRUE)
# OU: il est aussi possible d'appeller le csv stocké en ligne dans le dossier de données attaché a ce support en ligne. Il suffit de faire précéder le nom du fichier par le chemin vers le dossier de données : https://gitlab.com/formationsig/stat2/-/raw/main/donnees/
sexstat <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/114_rappel_sexe_stature.csv", stringsAsFactors = TRUE)
# regarder la structure du tableau
str(sexstat)
# on créee un objet taille contenant la variable stature
taille <- sexstat$stature
# résumé statistique de la variable
summary(taille)
# il manque l'écart-type
sd(taille, na.rm = TRUE)
# représentation graphique: la boîte à moustache
boxplot(taille
        , horizontal = TRUE
        , main = "distribution des statures (cm)"
)

# Ajouter la moyenne au graphique
points(mean(taille, na.rm=T), 1 # coordonnées x et y
       , pch = 18
       , col = "red"
       )

# Comparer 2 variables: stature et sexe
boxplot(taille~sexstat$sexe
        , horizontal = TRUE
        , main = "distribution des statures (cm)"
        , col = c("pink","yellow","blue")
        , ylab = "sexe"
	)
```

-----

### 2.1.  Conditions de validité remplies: La distribution d'une des deux variables suit une loi normale [correction]

```R
# J’ouvre mon tableau de données 
insee <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/211_insee_france_pop.csv")
#Je regarde la structure du tableau
str(insee)

## Étape 1 : Poser l’hypothèse H0
# La différence entre l'effectif des hommes et celui des femmes sur les 10 dernières années peut être expliquée par des variations aléatoires.

## Étape 2 : Définir le seuil de significativité
# je définis le seuil de significativité à 95% OU J'admets 5% d'erreur

## Étape 3 : Vérifier les conditions
# les distributions suivent elles une loi normale ?

# Méthode 1: L'histogramme
# tracer un histogramme de la variable "femmes"
hist(insee$femmes)  
# tracer un histogramme de la variable "hommes"
hist(insee$hommes)

# Méthode 2: Le diagramme Quantile-Quantile
# Instalation et utilisation de la bibliopthèque de fonction ggpubr
install.packages("ggpubr")
library(ggpubr)
# tracer du diagramme QQ de la variable "femmes"
ggqqplot(insee$femmes)+labs(title="femmes")
# tracer du diagramme QQ de la variable "hommes"
ggqqplot(insee$hommes)+labs(title="hommes")

# Méthode 3: Le test de Shapiro
shapiro.test(insee$femmes)

## Étape 4 : Choisir et réaliser le test : la corrélation de Pearson
# réaliser le test de correlation de Pearson
cor.test(insee$femmes,insee$hommes,method="pearson")

## Étape 5 : Lire le résultat
```

### 2.2.  Conditions de validité non remplies: la distribution d'une des deux variables NE suit PAS une loi normale [correction]

```R
## Objectif: tester la corrélation entre la profondeur des sépultures et l’altitude des fonds.

# importer le tableau
bavay <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/212_bavay_dim_fosse.csv")
# regarder la structure du tableau : 
str(bavay)

## Étape 1 : Poser l’hypothèse H0
# La corrélation que l’on peut supposer entre profondeur et altitude de fond des fosses sépulcrales est dûe au hasard.

## Étape 2 : Définir le seuil de significativité
#  On fixe le seuil de significativité 𝜶 à 0.05

## Étape 3 : Vérifier les conditions (normalité des distributions)
shapiro.test(bavay$alti_fond)
shapiro.test(bavay$prof)

## Étape 4 : Choisir et réaliser le test : la corrélation de Spearman
# L'une des variables ne suit pas une loi normale, je choisi donc de réaliser le test de corrélation de Spearman.
cor.test(bavay$alti_fond,bavay$prof,method="spearman")

## Étape 5 : Lire le résultat
```

### 2.3.  Exercice de restitution [correction]

#### <u>Correction version littérale:</u>

{% hint style='tip' %}

* Continuer de travailler sur le jeu de données **212_bavay_dim_fosse.csv**
* Décrire le tableau
* **Les longueurs et les largeurs de tombes sont liées ?**

{% endhint %}



**Est-ce que les longueurs et les largeurs de tombes sont liées ?**



**Étape 1 : Construire l’hypothèse nulle (*H<sub>0</sub>*) et l’hypothèse alternative (*H<sub>1</sub>*)**

> Formuler l’hypothèse nulle (*H<sub>0</sub>*) et l’hypothèse alternative (*H<sub>1</sub>*)

**"Le lien entre la longueur et la largeur des tombes est-il dû au hasard" ?**

> hypothèse nulle (*H<sub>0</sub>*): le hasard joue un rôle dans le lien entre les deux variables. 

 l'hypothèse alternative (*H<sub>1</sub>*), correspond à l’hypothèse que l'on veut démontrer:

* si *H<sub>0</sub>* est vrai alors on ne peut pas conclure sur une relation entre la longueur et la largeur des tombes
* si *H<sub>0</sub>* est faux alors il y a bien un lien entre la longueur et la largeur des tombes

On accepte ou rejette *H<sub>0</sub>*/on admet ou réfute **qu'il n'y a pas** de relation entre la longueur et la largeur des tombes.

**Étape 2 : Choisir le seuil de significativité**

> On choisit un seuil de significativité 𝜶 à 0.05

**Étape 3 : Vérifier les conditions**

> Vérifier la distribution des 2 variables **longueur** et **largeur** 

**Étape 4 : Choisir le test adapté**

> La distribution des 2 variables **longueur** et **largeur** suivent une loi normale → d'après l'[arbre de décision](lien_fiche_technique arbre decision) je choisi le test de corrélation de Spearman

**Étape 5 : Réaliser le test**

> le test se fait avec la ligne de commande `cor.test(x,y,method ="spearman")`

```
Spearman's rank correlation rho

data:  dimfos$larg and dimfos$long
S = 10059, p-value = 3.221e-12
alternative hypothesis: true rho is not equal to 0
sample estimates:
      rho 
0.7467021 

Warning message:
In cor.test.default(dimfos$larg, dimfos$long, method = "spearman") :
  Impossible de calculer la p-value exacte avec des ex-aequos
```

**Étape 6 : Lire le résultat**

> :eyes: la p-value est "toute petite", le résultat de la corrélation est donc significatif. Avec rho=0,746 : la corrélation est plutôt élevée.

→ Quand la corrélation est élevée, il est possible de rechercher la “loi” (ou formule) qui lie les deux variables.

#### <u>Correction - script:</u>

```R
## Objectif: tester la corrélation entre la longueur et la largeur des des sépultures.

# importer le tableau
bavay <- read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/212_bavay_dim_fosse.csv")
# regarder la structure du tableau : 
str(bavay)

## Étape 1 : Poser l’hypothèse H0# Les longueurs et les largeurs de tombes sont liées ?
# hypothèse nulle (H0): le hasard joue un rôle dans le lien entre les deux variables.

## Étape 2 : Définir le seuil de significativité
#  On fixe le seuil de significativité 𝜶 à 0.05

## Étape 3 : Vérifier les conditions (normalité des distributions)
shapiro.test(bavay$long)
shapiro.test(bavay$larg)

## Étape 4 : Choisir et réaliser le test : la corrélation de Pearson
# L'une des variables ne suit pas une loi normale, je choisi donc de réaliser le test de corrélation de Spearman.
cor.test(bavay$long,bavay$larg,method="spearman")

## Étape 5 : Lire le résultat
```

### 2.4.  Aller plus loin: Calculer la formule de corrélation et la représenter graphiquement [correction]



```R
## Créer le nuage de points
plot(bavay$long,bavay$larg)
# le 1er argument = axe des abscisses & le 2nd argument = axe des ordonnées

## Tracer La droite de régression
# avec la fonction lm (linear model) 
# le premier argument est la variable placée en ordonnées.
lm(bavay$larg~bavay$long)
# Représentation graphique
# Calculer des coordonnées de la droite de régression
reg1<-lm(bavay$long~bavay$larg)
# Tracer le nuage de points
plot(bavay$larg,bavay$long)
# Ajouter la droite
abline(reg1,col=”red”)

# installer et appel de la bibliothèque de fonctions car (Companion to Applied Regression)
install.package("car")
library(car)
# Tracer la droite de regression et +
scatterplot(long~larg, data=bavay)

## Bonus: Ajouter un critère discriminant
# Transformer la variable age_simpl(ifié) en variable catégorielle avec as.factor()
bavay$age_simpl <- as.factor(bavay$age_simpl)
# Tracer le nuage de points avec le critère age_simpl comme variable pour la couleur des points
plot(bavay$long,bavay$larg,
     pch=18,
     col=c("blue","orange","green")[bavay$age_simpl]
    )
# Ajouter une légende
legend("topleft",
       legend =c("NA","Adulte","Immature"),
       fill=c("blue","orange","green")
      )
# sous-échantillonner la série en ne gardant que les immatures
immat <- subset(bavay,age_simpl=="immature")
# Calculer la droite de régression pour les immatures...
reg2 <- lm(immat$larg~immat$long)
# ... et l'ajouter au graphique
abline(reg2,col="green")
# idem avec un sous-échantillon des adultes
adulte <- subset(bavay,age_simpl=="adulte")
reg3 <- lm(adulte$larg~adulte$long)
abline(reg3,col="orange")

# tests de normalité pour les 2 sous-échantillons
# pour les immatures:
shapiro.test(immat$long)
shapiro.test(immat$larg)
# → Les 2 variables suivent une loi normale → Pearson
# test de corrélation 
cor.test(immat$long,immat$larg,method = "pearson")
# pour les adultes:
shapiro.test(adulte$long)
shapiro.test(adulte$larg)
# → Une des variables ("adulte$long") a une distribution qui NE suit PAS une loi normale → Spearman
# test de corrélation 
cor.test(adulte$long,adulte$larg,method = "spearman")

# sous-échantillonage des adultes dont les fosses sépulcrales ont une longueur entre 150 et 280 cm
adulte2 <- subset(adulte,long>150&long<280)
# test de normalité
shapiro.test(adulte2$long)
shapiro.test(adulte2$larg) # → p-value supérieure au seuil de significativité (𝜶=0,05) → Spearman
# test de corrélation 
cor.test(adulte2$long,adulte2$larg,method="spearman")
```

#### 2.4.3. Lire une ACP [script]

```R
# Importer les données
acp<-read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/243_acp_notes_matieres.csv")
# Installer et activer les bibliothèques de fonction
install.package("FactoMineR")
library(FactoMineR)
install.package("explor")
library(explor)
# Calculer l'ACP avec FactomineR
res.PCA<-PCA(acp, quali.sup=1, graph=FALSE)
# Explorer l'ACP avec exploR
explor(res.PCA)
```
----

# 3. Deux séries d’une même variable quantitative

```R
### 3.1 Dans le cas où les conditions sont remplies

# Charger le tableau élémentaire et observer les variables:
alti <-  read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/310_altitude_phase.csv", stringsAsFactors = T)
str(alti)
# Transformer la variable "phase" en variable factorielle
alti$phase <- as.factor(alti$phase)
# demander un résumé des variables:
summary(alti)

## Étape 0 : (facultative) Représentation graphique
# diagramme de Cleveland (dotplot)
install.packages("lattice")
library("lattice")
dotplot(alti$z_inf~alti$phase
        , xlab = "Phase chronologique"
        , ylab = "Alti inférieure (m NGF)"
        )
# Boîte à moustache
boxplot(alti$z_inf~alti$phase
        , xlab = "Phase chronologique"
        , ylab = "Alti inférieure (m NGF)"
        )

## Étape 1 : Construire l’hypothèse nulle et l’hypothèse alternative
# Il y a-t'il une différence significative entre les altitudes inférieures des tombes de la phase 3 et celles de la phase 4 ?
# H0 = La différence d’altitude que j’observe entre les niveaux de repos des défunts des phases chronologiques 3 et 4 est due au hasard** (les deux moyennes sont égales)

## Étape 2 : Choisir le seuil de significativité
# le seuil de signification peut être fixé à 99% (α=0,01) ou 95% (α=0,05)

## Étape 3 : Choisir le test adapté
# Pour utiliser un test de Student, 2 conditions préalables doivent être remplies :
# * Condition 1: la distribution des deux séries doit suivre une loi normale.
# * Condition 2: les variances des deux séries doivent être égales.

# Condition 1: la distribution des deux séries doit suivre une loi normale.

# Vérifier la normalité des 2 séries "visuellement"
# avec un histogramme
hist(alti$z_inf[alti$phase==3]
     , xlab = "altitudes inférieures des tombes"
     )
hist(alti$z_inf[alti$phase==4]
          , xlab = "altitudes inférieures des tombes"
     )
# avec un QQplot (version avec Intervalle de confiance)
# install.packages("ggpubr")
library(ggpubr)
ggqqplot(alti$z_inf[alti$phase==3])
# Vérifier la normalité des 2 séries avec le test de Shapiro-Wilk
shapiro.test(alti$z_inf[alti$phase==3])
shapiro.test(alti$z_inf[alti$phase==4])

# Condition 2: homoscédasticité = les variances des deux séries doivent être égales.
# pour calculer la variance des 2 séries (de l'alti inf pour chaque phase), on utilise la fonction by
by(alti$z_inf   # variable à étudier
   ,alti$phase  # variable qui détermine les sous-groupes
   ,var         # fonction à calculer (ici la variance)
   , na.rm = T  # on ne prend pas les valeurs manquantes (au cas où...)
  )
# puis pour tester leur égalité on utilise le test F de Fisher
var.test(alti$z_inf[alti$phase==3],alti$z_inf[alti$phase==4])
# méthode plus empirique de B.Falissard (A vérifier)
paste("La variance de l'alti inf de la phase 3 =", var(alti$z_inf[alti$phase==3]), " doit être inférieure à 1,5 fois la variance de l'alti inf de la phase 4 (", var(alti$z_inf[alti$phase==4])*1.5, ")", sep ='')
paste("La variance de l'alti inf de la phase 4 =", var(alti$z_inf[alti$phase==4]), " doit être inférieure à 1,5 fois la variance de l'alti inf de la phase 3 (", var(alti$z_inf[alti$phase==3])*1.5, ")", sep ='')
# Quand les 2 conditions sont remplies (normalité des distributions et homoscédasticité), on peut lancer le test T de Student:
t.test(alti$z_inf[alti$phase==3]  # variable 1
      ,alti$z_inf[alti$phase==4]  # variable 2
      ,var.equal = TRUE)          # résultat de la condition 2

## Étape 4 : Lire le résultat
# si p-value < au seuil de signification, H0 est rejetée

###  3.2. Dans le cas où les conditions NE sont PAS remplies
##  3.2.1. Quand les variances des 2 séries ne sont pas égales (condition 2 non remplie) : on utilise la “correction de Welch”.
t.test(alti$z_inf[alti$phase==3]
       ,alti$z_inf[alti$phase==4]
       ,var.equal = FALSE    # condition 2 non remplie
      ) 
## 3.2.2. Quand au moins une des deux séries ne se distribue pas selon la loi normale (ou est constituée par de très petits effectifs ) (condition 1 non remplie) : on utilise le test de Wilcoxon-Mann-Whitney.
# Test de la condition 1 (normalité de la distribution - exemple pour la phase 2)
ggqqplot(alti$z_inf[alti$phase==2])
shapiro.test(alti$z_inf[alti$phase==2])
# distribution normale rejetée donc test de Wilcoxon
wilcox.test(alti$z_inf[alti$phase==2],alti$z_inf[alti$phase==3])
```

## 3.5. Pour aller plus loin : l'ANOVA

```R
# ANOVA

# Charger le tableau élémentaire et observer les variables:
masse_loc <-  read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/350_culot_masse_localisation.csv", stringsAsFactors = T)
str(culot)
# On fait un résumé
summary(culot$masse)
sd(culot$masse)
# Des boîtes à moustaches pour comparer les distributions de masse par localisation
boxplot(culot$masse~culot$localisation
        , xlab = "Localisation"
        , ylab = "masse des culots (en g)"
        )
# Analyse Of Variance
aov(culot$masse~culot$localisation)
# Pour lire le résultat
summary(aov(culot$masse~culot$localisation))
```


## 4.1. Rappels: deux variables qualitatives

```r
# répertoire de travail
setwd("C:/Stat_22/donnees_exercices")
# import du fichier
ceram <- read.csv2("410_ceram_categorie_fonction.csv")

# tableau de dénombrement (avec les fréquences absolues)
table(ceram$Fonction)
# tableau de dénombrement (avec les fréquences relatives)
prop.table(table(ceram$Fonction))
# ou
table(ceram$Fonction)/length(ceram$Fonction)*100

# Représentation graphique
# diagramme en barre
barplot(table(ceram$Fonction))
# camembert
pie(table(ceram$Fonction), col=c("white","grey","black"), main="catégorie fonctionnelle de céramique \n (n=1331)")
```
## Application: tester le lien entre deux variables qualitatives

```R



```
