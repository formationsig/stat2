# ![mod21_bivariee](images/stat21.png) module stat 2.1 - Comparer plusieurs séries de données : Y-a-t-il une relation entre les séries (statistiques) ?


Ce module socle est destiné à tous les agents ayant des données sous la forme de tableau et désirant comparer deux variables. 

**objectifs:** 

* Savoir caractériser ses données (variables qualitatives / quantitatives…).
* Comparer deux séries de données et la nature de leur relation en utilisant les outils statistiques adaptés aux questions posées.
* Choisir les outils statistiques qui conviennent le mieux à ses données.

**logiciels utilisés:** essentiellement [![R](images/R_logo_24.png)](https://cran.r-project.org/) avec l'interface [![Rstudio](images/Rstudio_logo_32.png)](https://www.rstudio.com/products/rstudio/download/)

**durée**: 5  jours

**prérequis:** Avoir suivi [les 3 modules de la formation stat 1](https://formationsig.gitlab.io/toc/#sig-31-bases-de-donnees-spatiale-et-attributaire) c'est à dire maitriser le vocabulaire et les notions de statistique descriptive univariée **et** avoir déjà utilisé le logiciel R à l'aide de l'interface Rstudio.

 

##  Ressources:

* Le diaporama associé à ce module *-en cours d'élaboration-* est disponible en ligne :

[![Diaporama](images/Diaporama_stat21.png)](https://slides.com/archeomatic/stat2/)

* Le [support de formation](https://formationsig.gitlab.io/stat2/) en ligne (existe aussi en version PDF, cf. lien dédié en haut de page). :construction: en cours de rédaction et mise en ligne !
* Pour accompagner le support il existe un [document contenant toutes les corrections et scripts R des exercices](https://stat2-formationsig-c72fd76af341b857a5a9815801d3f37b143340d8140d.gitlab.io/mod21_stat_bivariee_correction.html).
* Les [données mobilisées](https://gitlab.com/formationsig/stat2/-/tree/main/donnees) sont aussi accessible en ligne.

[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)