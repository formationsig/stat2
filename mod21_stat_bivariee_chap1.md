# Module stat 2.1 - Comparer plusieurs séries de données : Y-a-t-il une relation entre les séries (statistiques) ?





--------------------------------------

### Les encarts

Dans ce déroulé vous trouverez 4 types d'encarts:

{% hint style='info' %}
 :information_source: Information : indique une information d'ordre général
{% endhint %}

{% hint style='danger' %}
Attention: indique une information importante ou un problème récurent
{% endhint %}

{% hint style='working' %}
Astuce: indique une astuce, un raccourci
{% endhint %}

{% hint style='tip' %}
Exercice: indique des manipulations à faire ou l'énoncé d'un exercice
{% endhint %}

{% hint style='zob' %}
Autre: exemples, aparté, information facultative
{% endhint %}

---------------------

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

# 1. Rappels, vocabulaire et théorie

## 1.1. Rappel du socle 1 (types de variables, opérateurs de position, de dispersion)



La statistique permet de (se) **représenter** un ensemble de données souvent trop nombreuses pour être directement intelligibles. Pour cela on résume, représente, synthétise ces données sous forme de tableaux, d’indicateurs et de graphiques en fonction de leur type. Attention, la statistique reste un outil ! Ce n’est pas la réponse à toutes les interrogations quantitatives, et son utilisation peut dans certains cas s’avérer inappropriée voire incorrecte. Il faut réfléchir en amont à ce que l’on veut comparer et si cela s’avère pertinent. Les statistiques doivent rester une aide au diagnostic.


### 1.1.1. Vocabulaire



[![diapo 1.1.1.](images/Dia_111.png "diapo 1.1.1")](https://slides.com/archeomatic/stat_22/fullscreen#/1)



**Population**

La population est l’ensemble de tous les individus qui présentent le ou les caractères à étudier. Il est toujours important de définir clairement la population (sa source, ses caractéristiques…).



**Echantillon**

L’échantillon est un sous-ensemble de la population, sur lequel nous allons faire les études statistiques. La difficulté principale en archéologie est que cet échantillon peut être très réduit et que nous ne maîtrisons pas les modalités d’échantillonnage.



**Individu**

Les individus composent la population (fait, céramique, sépulture…). Ce sont sur ces individus que l’on va mesurer ou observer des phénomènes. Un individu correspond à une ligne dans le tableau.

**Variable ou caractère**

On appelle caractère ou variable une propriété commune observable à tous les individus d’une population statistique. On appelle variable une série de mesures ou d'observations effectuées sur un échantillon. Elle correspond à une colonne et une seule dans le tableau.



**Modalité**

Une **modalité** est la valeur ou la situation prise par une variable pour un individu. Ça correspond en général à une case dans le tableau.



**Tableau élémentaire**

C’est un tableau à double entrée dans lequel les lignes correspondent aux individus et les colonnes aux variables.



**Robustesse**

En statistiques, la robustesse d'un estimateur est sa capacité à ne pas être perturbé par une modification dans une petite partie des données, à une variation dans l’échantillonnage (valeur aberrante, non-normalité des distributions, variante non égales…).



**Fiabilité**

C'est un indicateur de confiance. Il sert à confirmer que les différences enregistrées entre les versions testées ne sont pas le fruit du hasard.



**Puissance**

La puissance statistique d'un test est la probabilité de rejeter l'hypothèse nulle. Plus un test est puissant, plus il permettra de mettre en évidence une différence entre deux distributions ayant des effectifs faibles.



**Variance**

La variance est une mesure qui permet de tenir compte de la dispersion de toutes les valeurs d'un ensemble de données par rapport à la moyenne.



**Tableau de dénombrement**

Le tableau de dénombrement donne un résumé numérique d'une distribution statistique. La construction du tableau de dénombrement et des représentations graphiques sera différente selon que le caractère étudié est quantitatif discret, quantitatif continu, ou qualitatif.



### 1.1.2. Nature d'une variable



Lorsque l’on travaille sur des données l’une des premières choses à vérifier est le type de variable auquel on est confronté car à chaque type de variable correspond des outils statistiques différents.

Il existe 2 types de variables : les qualitatives et les quantitatives.



**Variable qualitative**

On reconnaît une variable qualitative lorsque les modalités appartiennent à des catégories.

Elles peuvent être ordonnées ou non. (exemple : préhistoire-protohistoire-antiquité ou la description des US, l’orientation des sépultures…)



**Variable quantitative**

Une variable numérique (aussi appelée variable quantitative) est une caractéristique quantifiable dont les valeurs sont des nombres, à l'exclusion des nombres qui correspondent en fait à des codes. Ces valeurs sont mesurées.



{% hint style='tip' %}
A partir du tableau élémentaire **112_rappel_amboise_ceram.ods**:

- Définir la population ?  Définir l’échantillon ? Quels sont les individus ? 

- Déterminer la nature des variables ? 

  *<u>Note:</u> Chaque stagiaire doit déterminer la nature d'une variable (=colonne) en argumentant*

{% endhint %}



###  1.1.3. La distribution des données quantitatives et la loi normale



**Distribution**

Une distribution correspond à la répartition dans son ensemble de toutes les valeurs prises par une variable. Elle permet de résumer l'information contenue dans un ensemble de données. Attention, ce n’est pas un graphique !!!
Mais on peut représenter cette distribution de manière graphique par exemple avec une boite à moustache, un histogramme…

Il y a plusieurs types de distributions qui peuvent être caractérisées par leur forme (leurs paramètres).
Une distribution peut être uniforme, symétrique/asymétrique, unimodale/plurimodale et sa forme (notamment visible sur sa représentation graphique) permet de la rapprocher d’une loi de probabilité.



**La loi normale**

Parmi les lois de probabilité, il y a la loi normale qui résulte de la reproduction de plusieurs erreurs aléatoires, dans un processus naturel ou artificiel.

Sa représentation graphique “en cloche”, aussi nommée courbe de Gauss fut théorisée au 17e siècle lorsque les mathématiciens s’intéressent aux lois de probabilité avec pour modèle les jeux de hasard (dés, pile/face..).Sa formule est : 	

![formule_gauss](images/formule_gauss.png "formule de la loi normale")

Elle est définie par deux paramètres : l’espérance (μ, qui correspond à la moyenne) et l’écart-type (σ).

Sa modélisation fut réalisée à partir de la répétition d’expérience aléatoire à deux solutions (loi binomiale) comme un tirage à pile ou face ou plusieurs tirs de dés.

A chaque tirage la probabilité est égale entre les résultats possibles et au fur et mesure des tirages, la probabilité d’avoir x fois le même résultat est de plus en plus faible (proportionnelle aux solutions possibles).



[![video_planche_de_galton](images/video_galton.png "vidéo planche de Galton")](https://youtu.be/Ej0xIqr-RcI)



Cette fonction a des propriétés particulières qui permettent de connaître la répartition du caractère observé dans un échantillon à partir de la moyenne et de l’écart-type : 

- La moyenne + ou - 1 écart-type contient 68 % des individus (environ ⅔)
- La moyenne + ou - 2 écart-type contient 95% des individus
- La moyenne + ou - 3 écart-type contient 99 % des individus

![croquis_gauss](images/croquis_gauss.png "croquis de la courbe de Gauss")

La surface sous la courbe correspond à ce nombre d'individus. La lecture de cette courbe ou le calcul moyenne +/- écart-type permet de connaître la plage de valeur dans laquelle se trouve probablement une certaine proportion des individus.



**Le théorème centrale limite** :

Ce théorème mathématique démontre que la somme d’une suite de variable aléatoire converge vers une loi normale : en clair, si on répète une expérience un grand nombre de fois, on obtient une distribution normale même si la loi de départ est autre.



### 1.1.4. Rappel : utilisation de R

Le [Stat1 - module socle 1.2 - Initiation au traitement de données sous R](https://formationsig.gitlab.io/stat1/mod1_2_initR/mod1_2_initR.html) vous a permis de découvrir le langage R avec l'interface RStudio, c'est à dire importer un tableau, isoler une variable, traiter cette variable et la représenter.



{% hint style='working' %}
Si nécessaire, installer dans l'ordre le logiciel  [![R](images/R_logo_24.png)](https://cran.r-project.org/) puis  [![Rstudio](images/Rstudio_logo_32.png)](https://www.rstudio.com/products/rstudio/download/) en cliquant sur leurs icônes respectifves dans cet encart.

avant d'ouvrir les logiciels vérifiez l'emplacement de votre répertoire de :file_folder: **travail** et copier le contenu du dossier :file_folder: *donnees* dans celui-ci !
{% endhint %}



{% hint style='tip' %}
<u>Exercice</u>: **Rappels sur les notions de base du logiciel Rstudio**

- Ouvrir Rstudio

- Définir le répertoire de travail

→  *soit depuis l'onglet [File] → [...] → Options → set working directory, soit en ligne de commande avec la fonction* `setwd()`

- Importer le tableau **114_sexe_stature.csv** dans un objet que vous appellerez **sexstat**

→ *soit depuis le panneau [Environment] → [Import dataset] soit en ligne de commande avec la fonction* `read.csv2()`

→ *Vérifier qu'il a bien été importé dans le panneau [Environment]*

* Visualiser la structure d'un tableau, le nom des variables de celui-ci ainsi que leur type/nature ?

→ *utiliser la fonction* `str()`

→ *Chaque nom de variable est précédée d'un* `$` *elle peut être "appelée" par la suite sous la forme* `nom_tableau$nom_variable`

* Isoler la variable *stature* dans un objet nommé **taille** et déterminer la nature de cette variable
* Afficher les principaux indicateurs statistiques (moyenne, médiane, quartiles avec une seule fonction et l'écart-type avec la fonction dédiée) de la variable *stature*

→ <u>indice:</u> en anglais résumé ce dit *summary* et écart-type *standard deviation* 

→ *Si un calcul/une fonction renvoie* `NA` *il faut lui ajouter le paramètre* `na.rm = TRUE`

* Enfin, faire une représentation graphique appropriée de la variable *stature*

> :information_source: correction en fin de support


{% endhint %} 

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

## 1.2. La statistique bivariée : un peu de théorie

[![diapo 1.2](images/Dia_120.png "diapo 1.2")](https://slides.com/archeomatic/stat_22/fullscreen#/3)

### 1.2.1. Introduction



* **La statistique bivariée : pourquoi ?**

  

Si les analyses uni-variées permettent de résumer/décrire une grande série de données en quelques chiffres ou en une figure, les analyses bi-variées aident à la décision quand on se demande, par exemple, s’il existe une relation (une influence, une dépendance) entre deux variables.

L’objectif de la formation est donc de présenter des tests statistiques que nous pensons élémentaires pour que : 

* les agents puissent bien choisir celui dont ils ont besoin ;

* les agents puissent les mener à bien et, surtout, en tirer les bonnes conclusions.	



* **L’analyse bivariée : comment ?**

  

La première étape du choix du test demande de bien identifier **la nature des deux variables** qui seront confrontées. 



{% hint style='danger' %}

La **combinaison des variables** peut être : 

* **2 variables quantitatives** (les tailles et poids des stagiaires, les longueurs et largeurs de fosses, les volumes des silos et la distance à l’habitat, …) ;
*  **1 variable quantitative** mais observée sur deux sites ou à deux périodes différentes (l’âge des agents dans deux inter-régions différentes ; l’altitude des fonds de TP entre les zones 2 et 3, …) ;
* **1 variable qualitative et 1 quantitative** (le sexe et le salaire des agents, le type de site et la superficie des bâtiments, …) ;
* **2 variables qualitatives** (le sexe et la catégorie des agents, le type de vase et les phases chronologiques, …).

{% endhint %}



Ces différentes combinaisons sont la clé du déroulé de cette semaine de formation qui reprendra chacune d’entre elles et les tests qui lui sont attachés.
Ces tests statistiques que nous avons choisis appartiennent à la famille dite des “tests d’hypothèses”. Ils se présentent tous sous une même forme ; ils suivent tous la même procédure, avec, en premier lieu la formulation d’une hypothèse. Cette formulation est capitale car elle conditionne la réponse du test et  les conclusions que l’on peut tirer de ce dernier ! 

On va là encore utiliser des tests d'hypothèses car les tests d’hypothèse permettent d’aider à la validation d’hypothèses.
Ils permettent de réduire la subjectivité, en rendant les choix plus objectifs et plus transparents pour pouvoir les critiquer.



### 1.2.2. La procédure



- **1er étape : poser l’hypothèse de travail**

Il s’agit de transformer notre question de départ en une affirmation théorique qui sera testée par la statistique. C’est toujours cette affirmation qui est l’objet du test statistique, c’est-à-dire qu’après le test, on accepte ou non l’affirmation.

La transposition en affirmation ne se fait pas n’importe comment. En premier lieu, l’objectif est de **ne pas créer une association qui n’existe pas**. À l’instar de ce qu’il se passe dans le milieu judiciaire où il vaut mieux innocenter un coupable que condamner un innocent, il vaut mieux passer à côté d’un lien qui existe. On privilégie donc le rôle du hasard dans la constitution d’un corpus ou dans les différences que l’on peut soupçonnées entre deux variables.

Par extension, on appelle l’affirmation : l’**hypothèse nulle** ou, parfois, l’hypothèse du statu quo. Elle est noté *H<sub>0</sub>*.

Il existe, à l’opposé, une **hypothèse alternative** *H<sub>1</sub>* qui implique un effet quelconque, une différence notable.

Une seconde difficulté dans la formulation de *H<sub>0</sub>* est **de la tourner dans le but d’être rejetée** car il est plus sûr de reconnaître l’absence du rôle du hasard que son action (qui peut être très variable : faible, moyenne ou forte). 

Par exemple, dans le cas d’une hypothèse nulle qui dit : “Tous les cygnes sont blancs”, il est plus facile de dire “je ne sais pas si tous les signes sont blancs” que “oui, tous les cygnes sont blancs”. L ’absence de preuve (dans le jeu de données) n’est pas une preuve de l’absence (dans le monde entier). Il y a peut-être un cygne noir, bleu ou rose ailleurs. Un seul individu peut tout changer. En d’autres termes, il est plus facile de rejeter *H<sub>0</sub>* que de l’accepter.

**La formulation de l’hypothèse nulle est donc importante, puisqu’il faut la tourner pour rejeter avec certitude *H<sub>0</sub>* et non pas la valider avec des doutes.**

{% hint style='working' %}

En résumé, d’un point de vue pratique, pour la présente formation qui porte sur la recherche d’un lien entre deux variables, *H<sub>0</sub>* est une affirmation :

1. dans laquelle la corrélation observée est simplement due au hasard et 
2. qui dit l’inverse de ce que l’on souhaite trouver. 

{% endhint %}

*Exemples :* *H<sub>0</sub>* = “le lien est dû au hasard / à un problème d'échantillonnage” (*<u>Note:</u>* formulation à préférer à : “Il n’y a pas de lien entre les deux variables” car, en cas de rejet, il y aura une double négation pas facile à gérer dans la langue française).

La réponse du test sera, par conséquent : 

soit le rejet de **H<sub>0</sub>** : “Il y a un lien entre les deux qui n’est pas dû au hasard”; 

soit le non rejet : “on ne sait pas / on ne peut pas mesurer le rôle du hasard”.

L’analogie avec le milieu judiciaire peut, une nouvelle fois, être employée pour décrire cette prudence : “On accepte de conserver *H<sub>0</sub>* parce qu’on a pas pu accumuler suffisamment d’éléments matériels contre elle (pas de preuves suffisantes pour prouver quoi que ce soit). Accepter *H<sub>0</sub>*, c’est acquitter faute de preuves”. 

> *réf.biblio:* Daudin J.J., Robin S., Vuillet C. - Statistique inférentielle, idées, démarches, exemples. Rennes : Presses universitaires de Rennes, 1999, 192 p.(Coll. pratique de la statistique).



- **2e étape : définir le seuil de significativité**

{% hint style='danger' %}

Quand on pratique un test statistique avec *H<sub>0</sub>* défini, **deux erreurs sont possibles**.

La première est de <u>rejeter *H<sub>0</sub>* alors qu’elle est vraie</u> : on rejette le hasard, on pense trouver un lien entre 2 variables alors qu’il s’agit d’une coïncidence. On parle alors d’une **erreur de première espèce**, baptisée **alpha (α)**.

La deuxième erreur, dite de **erreur de seconde espèce** et baptisée **beta (β)**, est celle qui nous fait <u>accepter *H<sub>0</sub>* alors qu’elle est fausse</u> : il y a un lien (qui n’est pas du hasard) entre 2 variables mais on ne le voit pas.

{% endhint %}

Quand on utilise un test statistique, on souhaite tous être sûrs à 100% de notre résultat mais c’est malheureusement impossible. C’est alors au chercheur réalisant le test de définir le seuil (α puisque c’est la plus grave des deux, rappelez-vous de la 1ère étape) sous lequel il ne fait plus confiance à son test statistique. 

Classiquement, les seuils choisis sont α=0,01 ou α=0,05, c'est-à-dire être que l’on veut être respectivement sûr à 99% ((100-99)/100) ou à 95% de sa décision d’accepter ou rejeter *H<sub>0</sub>*.

La **puissance** statistique d'un test est l'aptitude à rejeter l'hypothèse nulle alors qu’elle est fausse. Donc plus un test est puissant, plus l’erreur de première espèce alpha (α) sera naturellement basse. Autrement dit, plus un test est puissant, plus il permettra de mettre en évidence une différence entre deux distributions, même pour les effectifs pouvant être faibles!



- **3e étape : choisir le test**

Comme annoncé précédemment, le choix du test dépend, en premier lieu, la nature des variables étudiées et confrontées. 

>  <u>*Note:*</u> Dans le cadre de la formation, on s’attardera sur chacune des combinaisons possibles. Mais gardez à l’esprit que seul, devant vos données, cette question s’imposera à vous.

Il dépend également de conditions “secondaires” comme : 

- la taille des effectifs à disposition (*car certains tests ne sont pas performant pour de petits effectifs*) ;
- ainsi que, dans le cas de variable quantitative, du type de la distribution des individus (*vous avez peut-être déjà entendu parler de tests paramétriques ou non-paramétriques ?*).

>  <u>*Note:*</u> Ces conditions dites de validité seront développées dans chaque partie.

La **robustesse** d’un test est sa capacité à donner un résultat fiable malgré des erreurs de saisie ou des conditions de validités non respectées. Dans le cas (rare en archéologie?) où le corpus est “conséquent”, tant en nombre d’individus (lignes du tableau de données) que de variables (colonnes), la probabilité augmente de rencontrer ces erreurs de mesures ou une plus grande variabilité dans chaque variable (variance). 

{% hint style='danger' %}

Les tests qui s’appuient sur la variance (comme le test de corrélation par exemple, ou l’AnOVa) peuvent rencontrer des soucis (vous aurez toujours un résultat mais la fiabilité de celui-ci sera faible) et sont considérés comme peu ou moins robustes. Pour éviter des désagréments, il est donc conseillé de faire attention aux valeurs aberrantes/outliers (Une boite à moustache peut aider a détecter les  éventuellement valeurs aberrantes).

{% endhint %}



- **4e étape : lire le résultat**



Il existe, aujourd’hui, deux façons de lire le résultat d’un test statistique.

- La première, celle originelle, *old school*, consiste à comparer le résultat du test (valeur observée) à une valeur théorique que l’on retrouve dans des tables habituellement fournies en annexe des manuels de statistique ([exemples de tables statistiques](https://www.supagro.fr/cnam-lr/statnet/tables.htm)). 

La lecture de ces tables de références nécessite de connaître 2 paramètres : **le seuil de signification alpha** (**α**, cf. 2e étape), en colonne, et **le degré de liberté** (***ddl*** ou ***df*** en anglais), en ligne.

Le **degré de liberté** désigne le nombre d’observations moins le nombre de paramètres à estimer entre ces observations. Il existe une formule simple pour calculer ce degré de liberté, (nbre de ligne-1)x(nbre de colonne-1), mais on y reviendra plus tard, au plus près des tests.

- La seconde manière de lire un résultat est plus simple d’utilisation. Elle est offerte par les logiciels de statistiques récents (R notamment) qui fournissent souvent une probabilité (**p-value** en anglais**, p-valeur, “probabilité critique” ou “petit p”** en français) qui “mesure l’accord entre l’hypothèse *H<sub>0</sub>* testée et le résultat obtenu. Plus [p-value] est proche de zéro, plus forte est la contradiction [avec] *H<sub>0</sub>* (...). Une p-value exactement nulle montre que le résultat obtenu est impossible si *H<sub>0</sub>* est vraie”. 

>  *réf.biblio:* Bennani Dosse M. - *Statistique bivariée avec R*, Rennes, Presses Universitaires de Rennes, 2011, p. 20 (coll. Pratique de la statistique). 

Dans la pratique, cela signifie que si **p-value < α** (toujours le pourcentage de significativité que nous sommes disposés à avoir), ***H<sub>0</sub>* est rejetée.**



- **5e étape : Tirer des conclusions**



{% hint style='danger' %}

On évite de faire dire n’importe quoi au test ! Reconnaître un lien entre deux variables au moyen d’un test n’explique pas le lien !

{% endhint %}



{% hint style='zob' %}

Il est ainsi possible de trouver un lien, dans des données médicales, entre “malade du cancer” et “consommation de choucroute” sans que cela veuille dire que manger de la “choucroute donne le cancer”. Simplement, le restaurant placé en face de l’hôpital pourrait, en fait, faire beaucoup de promotion sur le menu ”choucroute”.



De même, un lien peut exister entre les ventes de lunettes de soleil et celles de glaces. Pas parce que porter des lunettes de soleil donne une envie de fraîcheur sucrée, ni que manger une glace augmente notre sensibilité à la lumière. Il y a un bien un lien mais il s’agit d’un facteur tier qu’il faut rechercher dans un second temps : l’été, le soleil, … 



:information_source: Plusieurs de ces corrélations de l’absurde sont recensées sur le site https://tylervigen.com/spurious-correlations

{% endhint %}

