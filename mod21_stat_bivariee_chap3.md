# 3. Deux séries d’une même variable quantitative

[![diapo 3](images/Dia_300.png "diapo 3")](https://slides.com/archeomatic/stat_22/fullscreen#/5)

Quand on souhaite comparer :

- **une même variable quantitative sur deux sites différents** (<u>exemple</u>: les volumes des silos du site A *vs* ceux du site B → test d'homogénéité) ;
- **une même variable quantitative pour deux périodes chronologiques distinctes** (<u>exemple:</u> les volumes des silos de la phase 1 *vs* ceux de la phase 2 → test d'homogénéité) ;
- **une même variable quantitative par rapport à une seconde variable qualitative à deux modalités** (<u>exemple:</u> les volumes des silos piriformes *vs* ceux des coniques → test d'homogénéité) ;
- un échantillon par rapport à une population donnée (<u>exemple:</u> les volumes des silos du secteur 1 *vs* ceux du reste du site → test de conformité *de série non appariées ou d’échantillons indépendants*).

{% hint style='danger' %}
le choix du test utilisé va dépendre des conditions. Si elles sont remplies ou non.
{% endhint %}

## 3.1 Dans le cas où les conditions sont remplies

On utilise un grand classique des statistiques : le **test t de [Student](https://fr.wikipedia.org/wiki/Test_de_Student)** qui s’intéresse aux moyennes de chaque série.

{% hint style='tip' %}

* travailler avec le jeu de données **310_altitude_phase.csv**

  {% endhint %}

Charger le tableau élémentaire et observer les variables:

```R
alti <-  read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/310_altitude_phase.csv", stringsAsFactors = T)
str(alti)
```

```
'data.frame':	280 obs. of  4 variables:
 $ phase   : int  2 2 2 2 2 2 2 2 2 2 ...
 $ z_inf   : num  200 200 200 199 200 ...
 $ maturité: Factor w/ 2 levels "immature","mature": 1 1 1 1 1 1 1 1 1 2 ...
 $ phas_mat: Factor w/ 8 levels "2i","2m","3i",..: 1 1 1 1 1 1 1 1 1 2 ...
```

:eyes: Nous nous intéressons aux altitudes des niveaux de repos des squelettes (la variable "z_inf") selon les phases chronologiques reconnues (variable "phase")

:warning: Bien que les phases soient exprimées en chiffres, il ne s’agit pas d'une variable quantitative mais d'une variable qualitative ordinale → il faut d'ailleurs le spécifier dans le script en transformant la variable "phase" en variable factorielle (= catégorielle = qualitative)

```R
alti$phase <- as.factor(alti$phase)
```
On peut alors demander un résumé des variables:
```R
summary(alti)
```
```R
phase      z_inf           maturite      phas_mat 
 2:63   Min.   :198.5   immature: 63   5m     :61  
 3:75   1st Qu.:199.3   mature  :217   3m     :58  
 4:72   Median :199.4                  2m     :54  
 5:70   Mean   :199.4                  4m     :44  
        3rd Qu.:199.6                  4i     :28  
        Max.   :200.5                  3i     :17  
                                       (Other):18  
```
:eyes: Il y a 4 phases chronologiques: de la phase 2 à la phase 5 et on voudrait travailler sur les altitudes de fond de sépultures de chaque période.

On veut travailler sur 2 séries d'une même variable quantitative: les altitudes de la phase 3 et les altitudes de la phase 4.

**Étape 0 : (facultative) Représentation graphique**

Pour une première approche, on peut représenter graphiquement les 2 variables "z_inf" et "phase":

* avec un **diagramme de Cleveland** (fonction `dotplot` du package lattice)

```R
install.packages("lattice")
library("lattice")
dotplot(alti$z_inf~alti$phase
        , xlab = "Phase chronologique"
        , ylab = "Alti inférieure (m NGF)"
        )
```
![310_dotplot_phase_alti](images/310_dotplot_phase_alti.png)

* Ou une boîte à moustache:
```R
boxplot(alti$z_inf~alti$phase
        , xlab = "Phase chronologique"
        , ylab = "Alti inférieure (m NGF)"
        )
```

![310_boxplot_phase_alti](images/310_boxplot_phase_alti.png)

>:warning: on pourrait ici ajouter les moyennes vu que c'est ce que l'on compare ?! :warning:

:eyes: Sur ces représentations graphiques, nous observons des décalages en fonction de la phase chronologique *(:information_source: il n’y a pas de tombes en phase 1!)*. La question alors, est de savoir si : “est-ce que ce décalage est statistiquement significatif” ou “est-ce un effet du hasard / de l'échantillonnage ? ”.

{% hint style='info' %}

On remarque que cette question diffère sensiblement de celle que l’on s’est posée précédemment (avec le test de Pearson  [2.1. Rapport simple: 2 variables quantitatives](mod21_stat_bivariee.md#21-rapport-simple-2-variables-quantitatives)) : la question n'est plus : “y a-t-il un lien” (*on ne se demande pas si des altitudes sont liées à des altitudes !?*) mais “y a-t-il une différence réelle”! 

{% endhint %}

On parle ici d’un **test d’homogénéité** : Les altitudes de repos des défunts de la phase 3 et celles de la phase 4 sont-elles **homogènes**? 

**Étape 1 : Construire l’hypothèse nulle et l’hypothèse alternative**

Il est supposé que si deux moyennes diffèrent beaucoup, les deux échantillons considérés n’appartiennent pas à une même population.

{% hint style='info' %}

le test de Student ne s’applique que pour 2 séries de données à la fois (par paire). Dans l’exemple des altitudes de repos des squelettes, on ne peut comparer que 2 phases à la fois (phase 2 vs phase 3, puis phase 2 vs phase 4, puis phase 2 vs phase 5, *etc*).  *Spoiler : on peut comparer les altitudes de toutes les phases d’un coup avec l’ANOVA (3.4)!*

{% endhint %}

Dans le cas présent, on va comparer les altitudes de repos de la phase 3 avec celles de la phase 4.

Par conséquent, 

**H<sub>0</sub> = La différence d’altitude que j’observe entre les niveaux de repos des défunts des phases chronologiques 3 et 4 est due au hasard** (les deux moyennes sont égales).

Si H<sub>0</sub> est rejetée, alors H<sub>1</sub> : “Les moyennes des altitudes des deux phases chronologiques sont statistiquement différentes” est validée / “Les deux groupes d’individus statistiques proviennent de deux populations distinctes”, caractérisées par 2 moyennes différentes.

Si H<sub>0</sub> n’est pas rejetée, alors : “peut-être que la différence observée visuellement entre les altitudes des niveaux de repos des deux phases chronologiques est due au hasard (de l’échantillonnage)”. 

**Étape 2 : Choisir le seuil de significativité**

Dans un premier temps, on détermine le seuil de confiance qui nous rassure. Comme pour beaucoup de tests, le seuil de signification peut être fixé à 99% (α=0,01) ou 95% (α=0,05).

**Étape 3 : Choisir le test adapté**

{% hint style='danger' %}

Pour utiliser un test de Student, 2 conditions préalables doivent être remplies :

* Condition 1: la distribution des deux séries doit suivre une loi normale.
* Condition 2: les variances des deux séries doivent être égales.

{% endhint %}



**Condition 1: la distribution des deux séries doit suivre une loi normale.**

Nous l’avons vu précédemment, il s’agit là d’une condition incontournable de tous les tests statistiques pour distinguer les tests paramétriques des non paramétriques .

{% hint style='info' %}

On peut pourrait comparer la distinction entre tests paramétriques et tests non paramétriques *avec des voitures :car:  hauts de gamme et des entrées de gamme. :slightly_smiling_face:Les premiers ont des moteurs plus puissants et des options supplémentaires, notamment au niveau de la sécurité (plus d’airbags) mais ils coûtent plus cher. Dans les deux cas, les deux roulent et nous transportent où on le souhaite. Toutefois, si vous aviez le choix, lequel des deux véhicules prendriez-vous ?*

→ Le test de student est un test paramétrique. *Les coût supplémentaires de ce haut de gamme sont les 2 pré-requis / conditions à remplir !*

{% endhint %}

{% hint style='info' %}

Il est à noter que les tests de normalité sont sensibles à la taille des échantillons : plus les effectifs sont importants (n≥30) et plus la distribution tend à suivre naturellement une loi normale ([théorème central limite](https://fr.wikipedia.org/wiki/Théorème_central_limite)). D’un point de vue pratique, cela signifie que plus nos effectifs sont importants, moins nous avons besoin de faire attention à cette obligation de “normalité”.

→ :eyes: On peut regarder la distribution des individus dans les phases 3 & 4 avec un tableau de dénombrement

```R
table(alti$phase)
```

```R
 2  3  4  5 
63 75 72 70 
```

Les effectifs des deux séries qui nous intéressent (les individus des phases 3 & 4) sont supérieurs à 30. La condition de “normalité” est donc admise de fait. 

{% endhint %}

On peut néanmoins vérifier la normalité de distribution des 2 séries.

{% hint style='tip' %}

Vérifier la normalité des 2 séries étudiées (les altitudes de repos pour la phase 3 et celles de la phase 4):

* Visuellement (histogramme, diagramme QQ avec et/ou sans intervalle de confiance)
* A l'aide du test de Shapiro et Wilk

:information_source: correction en fin de support

{% endhint %}



* Vérifier la normalité des 2 séries avec un <u>histogramme</u>:

| `hist(alti$z_inf[alti$phase==3])`              |   `hist(alti$z_inf[alti$phase==4])`     |
| ---------------------------------------------- | ---- |
| ![310_hist_phase3](images/310_hist_phase3.png) |  ![310_hist_phase4](images/310_hist_phase4.png)    |

:eyes: La forme en cloche apparentée à une courbe de Gauss est plutôt reconnaissable, les distributions semblent être normales.

* Vérifier la normalité des 2 séries avec un <u>diagramme quantile-quantile (QQplot)</u>:

| `ggqqplot(alti$z_inf[alti$phase==3])`              |   `ggqqplot(alti$z_inf[alti$phase==4])`     |
| ---------------------------------------------- | ---- |
| ![310_QQ_phase3](images/310_QQ_phase3.png) |  ![310_QQ_phase3](images/310_QQ_phase4.png)    |

:eyes: Les points s’alignent : les distributions suivent une loi normale et restent dans l'intervalle de confiance à 95% (matérialisé par une bande grise, aide à la lecture). Les 2 séries semblent vraiment suivre une loi normale.

* Vérifier la normalité des 2 séries avec le <u>test de Shapiro et Wilk</u>:

| `shapiro.test(alti$z_inf[alti$phase==3])`              |   `shapiro.test(alti$z_inf[alti$phase==4])`     |
| ---------------------------------------------- | ---- |
| ![310_shapiro_phase3](images/310_shapiro_phase3.png) |  ![310_shapiro_phase4](images/310_shapiro_phase4.png)    |

:eyes: Interprétation : Les p-values > α=0,05, nous acceptons H<sub>0</sub> : 
**“les distributions de notre échantillon suivent une loi normale (et nous sommes confiant en notre choix à 95%)”** ! 

{% hint style='tip' %}

On peut aussi interpréter le test de Shapiro-Wilk avec la valeur du $W$ et le seuil de signification choisi dans [la table de Shapiro-Wilk ![icon_telechargement](images/icon_telechargement.png)](https://gitlab.com/formationsig/stat2/-/blob/33a80f2a60153f6e4e3adb184b14ec8ab622588b/tables_stat/table_shapiro_wilk.pdf).

:information_source:  le test de Shapiro et Wilk est le plus puissant des tests de normalité mais il fonctionne surtout pour les petits effectifs (n ≤ 50) 

> “Notez que, si la taille de votre échantillon est supérieure à 50, le graphique de normalité QQ plot est préféré parce qu’avec des échantillons de plus grande taille, le test de Shapiro-Wilk devient très sensible même à un écart mineur par rapport à la normale.” (https://www.datanovia.com/en/fr/lessons/test-t-dans-r/).

{% endhint %}



**Condition 2: les variances des deux séries doivent être égales (= [homoscédasticité](https://fr.wikipedia.org/wiki/Homoscédasticité)) .**

On compare les variances des 2 séries (avec la fonction `by`):

```R
by(alti$z_inf   # variable à étudier
   ,alti$phase  # variable qui détermine les sous-groupes
   ,var         # fonction à calculer (ici la variance)
   ,na.rm = T   # on ne prend pas les valeurs manquantes (au cas où...)
  )
```

```R
alti$phase: 2
[1] 0.08117005
------------------------------------------------------------------------ 
alti$phase: 3
[1] 0.072688
------------------------------------------------------------------------ 
alti$phase: 4
[1] 0.06606009
------------------------------------------------------------------------ 
alti$phase: 5
[1] 0.04655814
```

:eyes:  La première variance des alti de repos qui nous intéresse (Phase 3) est de 0,073 ; la seconde (Phase 4) est de 0,066. **Sont-elles égales ou non?**

On utilise le [test F de Fisher](https://fr.wikipedia.org/wiki/Test_de_Fisher_d'égalité_de_deux_variances) pour tester l'égalité des variances.

{% hint style='info' %}

Le test F de Fisher est un test parmi [d'autres test d'égalité des variances](https://www.datanovia.com/en/fr/lessons/test-dhomogeneite-des-variances-dans-r/). C'est un test d'hypothèse paramétrique; pour l'utiliser il faudra donc:

* définir H<sub>0</sub> : "Les deux variances sont égales".
* "payer le prix" du paramétrique c'est à dire la normalité des distributions (mais on l'a déjà fait pour la condition 1 :stuck_out_tongue_winking_eye: ).
* connaitre la ligne de commande **R**: `var.test(variable 1, variable 2)`

{% endhint %}

```R
var.test(alti$z_inf[alti$phase==3],alti$z_inf[alti$phase==4])
```

```R
	F test to compare two variances

data:  alti$z_inf[alti$phase == 3] and alti$z_inf[alti$phase == 4]
F = 1.1003, num df = 74, denom df = 71, p-value = 0.6864
alternative hypothesis: true ratio of variances is not equal to 1
95 percent confidence interval:
 0.6909905 1.7482206
sample estimates:
ratio of variances 
          1.100332 
```

:eyes: La p-value = 0,6864 étant supérieur au seuil de signification α=0,05, **on accepte H<sub>0</sub> et l’égalité des variances** (les deux échantillons sont extraits d’une même population ou de 2 populations qui ont la même variance).

{% hint style='tip' %}

On peut aussi interpréter le test  F de Fisher avec la valeur du $F$ et le seuil de signification choisi dans [la table de Fisher-Snedecor <img src="images/icon_telechargement.png" alt="icon_telechargement" style="zoom:67%;" />](https://gitlab.com/formationsig/stat2/-/blob/33a80f2a60153f6e4e3adb184b14ec8ab622588b/tables_stat/table_fisher_snedecor.pdf).

:eyes: Résultat *F* = 1,1003 et donc inférieur < *F* théorique : on accepte H<sub>0</sub>.  Par ailleurs, plus le ratio (ici = 1,10) est proche de 1 (en plus ou en moins), plus les variances sont proches.

{% endhint %}

{% hint style='tip' %}

Il existe une méthode plus empirique pour comparer les variances: c'est de considérer qu'un écart supérieur à 1,5 fois sa valeur signifie l'égalité des variances.

:warning:  :warning:  :warning: **pas clair ... à vérifier** :warning: :warning: :warning:    

> *<u>source</u>: [MOOC - Introduction à la statistique avec R de B. Falissard](https://www.fun-mooc.fr/fr/cours/introduction-a-la-statistique-avec-r/) - Chapitre 11 : Comparaison de deux moyennes.*

{% endhint %}



Quand **les 2 conditions sont remplies** (normalité des distributions et homoscédasticité), on peut lancer le test t de Student:

```R
t.test(alti$z_inf[alti$phase==3]  # variable 1
      ,alti$z_inf[alti$phase==4]  # variable 2
      ,var.equal = TRUE)          # résultat de la condition 2
```

```R
	Two Sample t-test

data:  alti$z_inf[alti$phase == 3] and alti$z_inf[alti$phase == 4]
t = -2.3851, df = 145, p-value = 0.01836
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.18963632 -0.01776923
sample estimates:
mean of x mean of y 
 199.3472  199.4509 
```

**Étape 4 : Lire le résultat**

:eyes: Avec une p-value à 0,01836 (inférieure au seuil de signification < α=0,05) **on peut rejeter H<sub>0</sub> : La différence observable entre les altitudes inférieures des sépultures de la phase 3 et celles de la phase 4 est significative** (:information_source: le test est significatif à 95% mais pas à 99% !)

{% hint style='tip' %}

On peut aussi interpréter le test  T de Student  avec:

*  Une p-value à 0,01836 (inférieure au seuil de signification< α=0,05), le résultat (t) est significatif (il y a 95% de chance de ne pas se tromper).
* On compare la valeur absolue du t observé ( t = 2.3851) doit être supérieur au t de référence dans la table de Student (à lire dans une des tables: [table de Student 1 <img src="images/icon_telechargement.png" alt="icon_telechargement" style="zoom:67%;" />](https://gitlab.com/formationsig/stat2/-/blob/33a80f2a60153f6e4e3adb184b14ec8ab622588b/tables_stat/table_student.pdf) /  [table de Student 2 <img src="images/icon_telechargement.png" alt="icon_telechargement" style="zoom:67%;" />](https://gitlab.com/formationsig/stat2/-/blob/33a80f2a60153f6e4e3adb184b14ec8ab622588b/tables_stat/table_student_2.pdf) avec le degré de liberté *ddl* (ou *df* en anglais, ici à 145) et le seuil de signification choisi (α=0,05).

:eyes: Dans le tableau à l'intersection de *dl* à 100 (ou plus) et de 0,05 on lit la valeur de t de référence=1,660. La valeur absolue de t observé = 2.3851 est supérieure, on peut donc rejeter H<sub>0</sub> → La différence observable entre les altitudes inférieures des sépultures de la phase 3 et celles des tombes de la phase 4 est significative.

{% endhint %}

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

## 3.2. Dans le cas où les conditions NE sont PAS remplies

### 3.2.1. Quand les variances des 2 séries ne sont pas égales (condition 2 non remplie) : on utilise la “correction de Welch”.

Il suffit de reprendre le test t de Student mais avec l'argument `var.equal = FALSE`

```R
t.test(alti$z_inf[alti$phase==3]
       ,alti$z_inf[alti$phase==4]
       ,var.equal = FALSE    # condition 2 non remplie
      ) 
```

```R
	Welch Two Sample t-test

data:  alti$z_inf[alti$phase == 3] and alti$z_inf[alti$phase == 4]
t = -2.3875, df = 144.99, p-value = 0.01825
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -0.18955205 -0.01785351
sample estimates:
mean of x mean of y 
 199.3472  199.4509 
```

:eyes: Avec une p-value inférieure à 0.05 on peur rejeter H<sub>0</sub> 

:warning:  :warning: :warning: ici on ne devrait pas le faire puisque les 2 distributions respectent les 2 conditions :warning: :warning:    



### 3.2.2. Quand au moins une des deux séries ne se distribue pas selon la loi normale (ou est constituée par de très petits effectifs ) (condition 1 non remplie) : on utilise le test de Wilcoxon-Mann-Whitney.

Si on voulait comparer les alti de repos de la phase 3 avec ceux de la phase 2. Il faut commencer par vérifier la condition 1 pour cette dernière (normalité de la distribution):

| `ggqqplot(alti$z_inf[alti$phase==2])`                        | `shapiro.test(alti$z_inf[alti$phase==2])`                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![320_QQ_phase2](images/320_QQ_phase2.png)                   | ![320_shapiro_phase2](images/320_shapiro_phase2.png)         |
| :eyes: ​ On voit bien des points hors de la zone grise ! la distribution ne suit pas une loi normale ! | :eyes: la p-value est inférieur au seuil de signification α=0,05, nous rejetons  H<sub>0</sub> et la “normalité de la distribution”. |

Si l’on veut tester la différence entre les altitudes du fond des sépultures de la phase 2 et celle de la phase 3, on utilise le test de [Wilcoxon-Mann-Whitney](https://fr.wikipedia.org/wiki/Test_de_Wilcoxon-Mann-Whitney) : 

```R
wilcox.test(alti$z_inf[alti$phase==2],alti$z_inf[alti$phase==3])
```

```R
	Wilcoxon rank sum test with continuity correction

data:  alti$z_inf[alti$phase == 2] and alti$z_inf[alti$phase == 3]
W = 1869, p-value = 0.03504
alternative hypothesis: true location shift is not equal to 0
```

:eyes: Avec une p-value à 0,03504 (inférieure au seuil de signification < α=0,05) **on peut rejeter H<sub>0</sub> : La différence observable entre les altitudes inférieures des sépultures de la phase 3 et celles de la phase 4 est statistiquement significative.** 

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

## 3.3. Test de Student: schéma de décision

[![330_schema_student](images/330_schema_student.png)](https://slides.com/archeomatic/stat_22/fullscreen#/5/9)

## 3.4. Exercice de restitution

{% hint style='tip' %}

Comparer les altitudes de fond entre les sépultures de la phase 4 et de la phase 5.

{% endhint %}

<div style="page-break-after: always; visibility: hidden"> \pagebreak </div>

## 3.5. Pour aller plus loin : l'ANOVA



L’[ANOVA](https://fr.wikipedia.org/wiki/Analyse_de_la_variance) (ANalysis Of VAriance) est une généralisation de l’approche précédente qui permet d’étudier la liaison entre une variable quantitative et plus de deux groupes. Une petite différence existe néanmoins : le test t de Student s’intéresse à la moyenne alors que l'anova, comme son nom l’indique, utilise la variance.

Student et anova appartiennent à la famille des tests paramétriques, c’est-à-dire que des conditions doivent être remplies pour pouvoir les utiliser pleinement (4 pour l’anova!). Le pendant non-paramétrique de l’anova est le test de Kruskal-Wallis. Cependant, sa robustesse (capacité à être pertinent malgré la violation de ces conditions) est telle, que nous ne nous attarderons pas sur l’étape de validation de dites conditions.

{% hint style='tip' %}

travailler avec le jeu de données **350_culot_masse_localisation.csv**

{% endhint %}

Charger le tableau élémentaire et observer les variables:

```R
# Charger le tableau élémentaire et observer les variables:
culot <-  read.csv2("https://gitlab.com/formationsig/stat2/-/raw/main/donnees/350_culot_masse_localisation.csv", stringsAsFactors = T)
str(culot)
```

```
'data.frame':	199 obs. of  2 variables:
 $ masse       : int  38 38 37 36 35 34 33 33 33 32 ...
 $ localisation: Factor w/ 3 levels "room_100","room_102",..: 2 2 2 3 2 3 3 2 2 1 ...
```

<u>Contexte et questionnement:</u> 

Lors de la fouille, deux espaces de forge ont été découverts (room_100 + space_101 et room_102). Ces derniers sont situés à deux endroits du site. L’étude a révélé qu’ils n’ont pas fonctionné ensemble et que l’un a succédé à l’autre. On sait que la masse des culots est liée à l’activité et à la production des objets en fer.

On veut donc savoir si le déplacement de la forge s’accompagne d’un changement dans les productions et donc s’il existe un lien entre la masse des culots et leur localisation. 

**Étape 1 : Construire l’hypothèse nulle et l’hypothèse alternative**

 {% hint style='tip' %}

Formuler l'hypothèse H<sub>0</sub> et H<sub>1</sub>

{% endhint %}

 {% hint style='zob' %}

**Hypothèse nulle (H<sub>0</sub>)** = Les distributions de chaque série (la masse des culots pour chaque localisation) suivent une même loi normale : le lien entre la masse et la localisation est dû au hasard.

**Hypothèse alternative (H<sub>1</sub>)** = Il existe au moins une distribution dont la moyenne s'écarte des autres moyennes : le lien entre la masse et la localisation serait statistiquement significatif. :bulb: **c'est un peu abstrait jeune homme...**

{% endhint %}

On cherche alors à visualiser l'éventuelle influence de la localisation du culot sur sa masse.

 {% hint style='tip' %}

* Quelle représentation permet de voir s’il y a une différence entre la masse des culots de la room_100, space_101 et room_102 ?
* Que peut-on en dire ?

{% endhint %}

 {% hint style='zob' %}

Une boîte à moustache !

```R
boxplot(culot$masse~culot$localisation
        , xlab = "Localisation"
        , ylab = "masse des culots (en g)"
        )
```

![350_anova_boxplot](images/350_anova_boxplot.png)

:eyes: Les masses ne semblent pas différentes, en effet les écarts ne sont pas très grands. 

{% endhint %}

La question sera de savoir si ces écarts sont significatifs ou pas? Pour cela nous allons tester la variance pour étudier l'influence ou non de la localisation sur les masses par une ANOVA.

**Étape 2 : Choisir le seuil de significativité**

On détermine le seuil de confiance qui nous rassure. Comme pour beaucoup de tests, le seuil de signification peut être fixé à 99% (α=0,01) ou 95% (α=0,05).

**Étape 3 : Choisir le test adapté**

L’ANOVA ou [analyse de la variance](https://www.techno-science.net/glossaire-definition/Analyse-de-la-variance.html) permet d'étudier le comportement d'une variable à expliquer en fonction d'une ou plusieurs variables explicatives catégorielles.

C’est un test statistique permettant de vérifier que plusieurs échantillons sont issus d'une même population, elle est utilisée quand on veut déterminer s'il existe une différence entre les moyennes de différents groupes.

Cela signifie qu’elle permet d’identifier une différence mais ne dit pas quels groupes spécifiques sont statistiquement différents les uns des autres.

```R
aov(culot$masse~culot$localisation)
```

```R
Call:
   aov(formula = culot$masse ~ culot$localisation)

Terms:
                culot$localisation Residuals
Sum of Squares                 463.593 14174.337
Deg. of Freedom                      2       196

Residual standard error: 8.504002
Estimated effects may be unbalanced
```

:eyes: Le résultat donné est justement la somme des carrés et le degré de liberté. Cela n'est pas très parlant ! Pour lire les résultats de l'ANOVA on va faire un résumé de celle-ci:

```R
summary(aov(culot$masse~culot$localisation))
```

```
                        Df Sum Sq Mean Sq F value Pr(>F)  
culot$localisation   2    464  231.80   3.205 0.0427 *
Residuals              196  14174   72.32                 
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

 {% hint style='tip' %}

Sachant que:

* Df = degré de liberté
* Sum Sq = Sommes des carrés
*  Mean Sq = Moyenne des carrés
*  F value = test de Fisher
   
*  Pr(>F) = p-value = 0.0427
   
* Que signifie ce résultat? 
* Quelle est la réponse à la question initiale : Y a-t-il un lien entre la localisation et la masse des culots ? 

{% endhint %} 

 {% hint style='zob' %}

:eyes: Avec une p-value à 0.0427 inférieure au seuil de signification  α=0,05 on ne peut rejeter l'hypothèse nulle H<sub>0</sub> alternative  H<sub>1</sub> est à dire qu'il n'y a pas de différence statistiquement significative entre la masse moyenne des culots selon leur localisation.

{% endhint %} 

:warning:  C'est une juste jeune homme !  A compléter ! ​..​.​ ​+​ ​un​ ​e​x​e​r​c​i​c​i​ce​ ?:warning: ​​

